package com.jcurbo.jhuep.en_605_421_31.hw4;

/** Define a Huffman coding tree */
public class Tree implements Comparable<Tree> {
  public Node root; // The root of the tree

  // Creates an empty tree
  public Tree() {
	  root = new Node();
	  root.left = null;
	  root.right = null;
	  root.weight = 0;
  }
  
  /** Create a tree with two subtrees */
  public Tree(Tree t1, Tree t2) {
    root = new Node();
    root.left = t1.root;
    root.right = t2.root;
    root.weight = t1.root.weight + t2.root.weight;
  }
  
  public Node getRoot()
  {
	  return root;
  }
  
  /** Create a tree containing a leaf node */
  public Tree(int weight, char element) {
    root = new Node(weight, element);
  }
  
  /** Compare trees based on their weights */
  public int compareTo(Tree o) {
    if (root.weight < o.root.weight) // Purposely reverse the order
      return 1;
    else if (root.weight == o.root.weight)
      return 0;
    else
      return -1;
  }

  public class Node {
    char element; // Stores the character for a leaf node
    int weight; // weight of the subtree rooted at this node
    Node left; // Reference to the left subtree
    Node right; // Reference to the right subtree
    String code = ""; // The code of this node from the root

    /** Create an empty node */
    public Node() {
    }
    
    /** Create a node with the specified weight and character */
    public Node(int weight, char element) {
      this.weight = weight;
      this.element = element;
    }
  }
  
  
}  