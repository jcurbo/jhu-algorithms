package com.jcurbo.jhuep.en_605_421_31.hw4;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Path;

public class Frontend 
{
	public static class MainWindow extends JPanel implements ActionListener
	{
		
		private static final long serialVersionUID = 1L;

		public static File file;
		
		JButton encButton, decButton;
		JFileChooser fc;
		JLabel iLabel1, iLabel2;
	
		MainWindow ()
		{
			fc = new JFileChooser();
			
			iLabel1 = new JLabel("Encode, then decode.  When decoding, choose encoded.txt.");
			iLabel2 = new JLabel("Decoded text will be in decoded.txt");
			
			encButton = new JButton("Encode");
			encButton.addActionListener(this);
			
			decButton = new JButton("Decode");
			decButton.addActionListener(this);
			
			JPanel buttonPanel = new JPanel(new GridLayout(0,1));
			buttonPanel.add(iLabel1);
			buttonPanel.add(iLabel2);

			buttonPanel.add(encButton);
			buttonPanel.add(decButton);
			
			add(buttonPanel, BorderLayout.PAGE_START);
					
			
		}
		
		public void actionPerformed(ActionEvent e) 
		{			
			if (e.getSource() == encButton) {
				int ret = fc.showOpenDialog(this);
				if (ret == JFileChooser.APPROVE_OPTION) {
					file = fc.getSelectedFile();
					encodeFile(file);
					JOptionPane.showMessageDialog(null,  "File encoded!", null, JOptionPane.INFORMATION_MESSAGE, null);
				}
			} else if (e.getSource() == decButton) {
				int ret = fc.showOpenDialog(this);
				if (ret == JFileChooser.APPROVE_OPTION) {
					file = fc.getSelectedFile();
					decodeFile(file);
					JOptionPane.showMessageDialog(null,  "File decoded!", null, JOptionPane.INFORMATION_MESSAGE, null);
				}
			}
		}
	}
	
	public static void encodeFile(File file) 
	{	
		byte[] fileContents = new byte[(int) file.length()];
		
		// Java 7 NIO version
		/*
		Path readFilePath = file.toPath();
		String newFileName = readFilePath.getParent().toString() + "/encoded.txt";
		
		try {
			fileContents = Files.readAllBytes(file.toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		// Java 6 IO version
		String absPath = file.getAbsolutePath();
		String filePath = absPath.substring(0,absPath.lastIndexOf(File.separator));
		String newFileName = filePath + "/encoded.txt";
		
		try {
			FileInputStream fileInputStream = new FileInputStream(file);
			fileInputStream.read(fileContents);
			fileInputStream.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		Huffman.encodeToFile(fileContents, newFileName);
	}
	
	public static void decodeFile(File file)
	{
		byte[] fileContents = new byte[(int) file.length()];
		
		// Java 7 NIO version
		/*
		Path readFilePath = file.toPath();
		String newFileName = readFilePath.getParent().toString() + "/decoded.txt";
		try {
			fileContents = Files.readAllBytes(file.toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		// Java 6 IO version
		String absPath = file.getAbsolutePath();
		String filePath = absPath.substring(0,absPath.lastIndexOf(File.separator));
		String newFileName = filePath + "/decoded.txt";
		
		try {
			FileInputStream fileInputStream = new FileInputStream(file);
			fileInputStream.read(fileContents);
			fileInputStream.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		Huffman.decodeToFile(fileContents, newFileName);
	}
	
	
	public static void createPanel()
	{
		JFrame frame = new JFrame("Huffman Coding");
		MainWindow app = new MainWindow();
		frame.add(app, BorderLayout.CENTER);
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setSize(400, 200);

    	frame.setLocationRelativeTo(null); // Center the frame
	    frame.setVisible(true);
	}
	
	public static void main(String[] args) 
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createPanel();
            }
        });
		
	}
	
}
