package com.jcurbo.jhuep.en_605_421_31.hw4;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Huffman {
	
	static int[] counts;
	static Tree tree;
	static String[] codes;

	// TODO - change to bytes instead of characters
	  /** Decode the bit string into a text */
	  public static String decode(String bits) {
	    String result = "";
	    	    	    
	    Tree.Node p = tree.root; // Start from the root
	    for (int i = 0; i < bits.length()-1; i++) {
	      if (bits.charAt(i) == '0') 
	        p = p.left;
	      else if (bits.charAt(i) == '1')
	        p = p.right;
	      else
	        return null; // Wrong bits
	      
	      if (p.left == null) { // A leaf detected
	        result += p.element;
	        p = tree.getRoot(); // Restart from the root
	      }
	    }
	    
	    return result;
	  }
	  
	  public static void decodeToFile(byte[] text, String outputFilename)
	  {	  
		  String bits = "";
		  String decodedBits = "";
		  
		  int decoded;
		  
		  for (int i = 0; i<text.length; i++) {
			 
			 decoded = ((int)text[i] << 24) >>> 24;
			 bits += String.format("%8s", Integer.toBinaryString(decoded)).replace(' ',  '0');
		  }
		  
		  decodedBits = decode(bits);
		  
		  File outputFile = new File(outputFilename);
		  if (!outputFile.exists()) {
			  try {
				outputFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  }
		  
		  try {
			  BufferedWriter bw = new BufferedWriter(new FileWriter(outputFilename));
			  bw.write(decodedBits);
			  bw.close();
		  } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }
		    
	  }
	  
	  
	  // TODO - change to bytes instead of characters
	  /** Encode the text using the codes */
//	  public static String encode(String text, String[] codes) {
//	    String result = "";
//	    for (int i = 0; i < text.length(); i++) 
//	      result += codes[text.charAt(i)];    
//	    return result;   
//	  }
	  
	  public static void encodeToFile(byte[] text, String filename)
	  {
		  BinaryOut bOut = new BinaryOut(filename);
		  int conv;
		  int r;
		  
		  counts = getCharacterFrequency(text);
		  tree = getHuffmanTree(counts);
		  codes = getCode(tree.root);
		  		  
		  for (int i = 0; i < text.length; i++) {
			  conv = Integer.parseInt(codes[text[i]], 2);
			  r = codes[text[i]].length();
			  
			  bOut.write(conv, r);
		  }
		  bOut.close();
	  }
	  	  
	  /** Get Huffman codes for the characters 
	   * This method is called once after a Huffman tree is built
	   */
	  public static String[] getCode(Tree.Node root) {
	    if (root == null) return null;    
	    String[] codes = new String[2 * 128];
	    assignCode(root, codes);
	    return codes;
	  }
	  
	  /* Recursively get codes to the leaf node */
	  private static void assignCode(Tree.Node root, String[] codes) {
	    if (root.left != null) {
	        root.left.code = root.code + "0";
	        assignCode(root.left, codes);
	        
	        root.right.code = root.code + "1";
	        assignCode(root.right, codes);
	    }
	    else {
	      codes[(int)root.element] = root.code;
	    }
	  }
	  
	  /** Get the frequency of the characters */
	  public static int[] getCharacterFrequency(byte[] text) {
	    int[] counts = new int[256]; // 256 ASCII characters
	    
	    for (int i = 0; i < text.length; i++)
	      counts[text[i]]++; // Count the character in text
	    
	    return counts;
	  }
	  
	  /** Get a Huffman tree from the codes */  
	  public static Tree getHuffmanTree(int[] counts) {
	    // Create a heap to hold trees
	    Heap<Tree> heap = new Heap<Tree>();
	    for (int i = 0; i < counts.length; i++) {
	      if (counts[i] > 0)
	        heap.add(new Tree(counts[i], (char)i)); // A leaf node tree
	    }
	    
	    while (heap.getSize() > 1) { 
	      Tree t1 = heap.remove(); // Remove the smallest weight tree
	      Tree t2 = heap.remove(); // Remove the next smallest weight 
	      heap.add(new Tree(t1, t2)); // Combine two trees
	    }

	    return heap.remove(); // The final tree
	  }  
	  
//	  public static String encodeString(String text)
//	  {
//		  int[] counts = Huffman.getCharacterFrequency(text);
//		  Tree tree = Huffman.getHuffmanTree(counts);
//		  int[] codes = Huffman.getCode(tree.root);
//		  String enc = Huffman.encode(text, codes);
//		  
//		  return enc;
//	  }
	  
	  //public static String decodeString(String text)
	  //{
		  
	  //}
}
