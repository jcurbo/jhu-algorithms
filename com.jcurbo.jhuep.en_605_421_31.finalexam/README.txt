README for 605.421, Fundamentals of Algorithms Final Exam
James Curbo
December 2, 2013

------------------
The solution to the exam is presented as one Eclipse project with multiple Java classes.

- Problem 1, Recursive maze traversal: RecursiveMazeTraversal.java
- Problem 2, Fibonacci numbers: ComputeFibonacci.java
- Problem 3, Fractals: Fractal.java, FractalJPanel.java
- Problem 4, Binary search trees: BST.java, BinarySearchTree.java, Tree.java (unchanged), AbstractTree.java (unchanged)
- Problem 5, Bridge: Bridge.java, DeckOfCards.java (used version from HW1)

Note on Problem 5:  Not completely sure I did it right, but instead of outputting a tree of some sort I simply output the result
of each matchup (step of the minimax or alpha-beta) to the console as well as the depth in the recursion. 

Running: Interface.java is the entry point and pops up a dialog asking which solution to run. 
- Problem 1 runs in the console with some text input necessary
- Problem 2 runs in a JTextArea with no input necessary
- Problem 3 runs in a JFrame with buttons
- Problem 4 runs in a JTextArea with no input necessary
- Problem 5 (both variants) runs in the console (no input)