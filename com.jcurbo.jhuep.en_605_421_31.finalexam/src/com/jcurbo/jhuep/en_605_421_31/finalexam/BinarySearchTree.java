/** Original code was provided by "Introduction to Java Programming" 
 * 8th Edition by Y. Daniel Liang, pages 858 - 869
 *
 *  This code is used for educational purposes only
 *  in 605.421 Foundations of Algorithms. 
 * 
 */

package com.jcurbo.jhuep.en_605_421_31.finalexam;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


public class BinarySearchTree<E extends Comparable<E>>
    extends AbstractTree<E> implements Cloneable {
  protected TreeNode<E> root;
  protected int size = 0;

  /** Create a default binary tree */
  public BinarySearchTree() {
  }

  /** Create a binary tree from an array of objects */
  public BinarySearchTree(E[] objects) {
    for (int i = 0; i < objects.length; i++)
      insert(objects[i]);
  }

  /** Returns true if the element is in the tree */
  public boolean search(E e) {
    TreeNode<E> current = root; // Start from the root

    while (current != null) {
      if (e.compareTo(current.element) < 0) {
        current = current.left;
      }
      else if (e.compareTo(current.element) > 0) {
        current = current.right;
      }
      else // element matches current.element
        return true; // Element is found
    }

    return false;
  }

  /** Insert element o into the binary tree
   * Return true if the element is inserted successfully */
  public boolean insert(E e) {
    if (root == null)
      root = createNewNode(e); // Create a new root
    else {
      // Locate the parent node
      TreeNode<E> parent = null;
      TreeNode<E> current = root;
      while (current != null)
        if (e.compareTo(current.element) < 0) {
          parent = current;
          current = current.left;
        }
        else if (e.compareTo(current.element) > 0) {
          parent = current;
          current = current.right;
        }
        else
          return false; // Duplicate node not inserted

      // Create the new node and attach it to the parent node
      if (e.compareTo(parent.element) < 0)
        parent.left = createNewNode(e);
      else
        parent.right = createNewNode(e);
    }

    size++;
    return true; // Element inserted
  }
  
  // Version from CORS 3rd Ed
  
  public void insert2(E e) {
	  if (root == null) {
		  root = createNewNode(e);
	  } else {
		TreeNode<E> y = null;
		TreeNode<E> x = root;
		
		TreeNode<E> z = createNewNode(e);
		
		while (x != null) {
			y = x;
			if (z.element.compareTo(x.element) < 0) {
				x = x.left;
			} else {
				x = x.right;
			}
		}
		
		if (y == null) {
			root = z;
		} else if (z.element.compareTo(y.element) < 0 ) {
			y.left = z;
		} else {
			y.right = z;
		}
		
	  }
	  
  }
  

  protected TreeNode<E> createNewNode(E e) {
    return new TreeNode<E>(e);
  }

  /** Inorder traversal from the root*/
  public void inorder() {
    inorder(root);
  }

  /** Inorder traversal from a subtree */
  protected void inorder(TreeNode<E> root) {
    if (root == null) return;
    inorder(root.left);
    System.out.print(root.element + " ");
    inorder(root.right);
  }

  /** Postorder traversal from the root */
  public void postorder() {
    postorder(root);
  }

  /** Postorder traversal from a subtree */
  protected void postorder(TreeNode<E> root) {
    if (root == null) return;
    postorder(root.left);
    postorder(root.right);
    System.out.print(root.element + " ");
  }

  /** Preorder traversal from the root */
  public void preorder() {
    preorder(root);
  }

  /** Preorder traversal from a subtree */
  protected void preorder(TreeNode<E> root) {
    if (root == null) return;
    System.out.print(root.element + " ");
    preorder(root.left);
    preorder(root.right);
  }

  /** Inner class tree node */
  public static class TreeNode<E extends Comparable<E>> {
    E element;
    TreeNode<E> left;
    TreeNode<E> right;

    public TreeNode(E e) {
      element = e;
    }
  }

  /** Get the number of nodes in the tree */
  public int getSize() {
    return size;
  }

  /** Returns the root of the tree */
  public TreeNode<E> getRoot() {
    return root;
  }

  /** Returns a path from the root leading to the specified element */
  public java.util.ArrayList<TreeNode<E>> path(E e) {
    java.util.ArrayList<TreeNode<E>> list =
      new java.util.ArrayList<TreeNode<E>>();
    TreeNode<E> current = root; // Start from the root

    while (current != null) {
      list.add(current); // Add the node to the list
      if (e.compareTo(current.element) < 0) {
        current = current.left;
      }
      else if (e.compareTo(current.element) > 0) {
        current = current.right;
      }
      else
        break;
    }

    return list; // Return an array of nodes
  }

  /** Delete an element from the binary tree.
   * Return true if the element is deleted successfully
   * Return false if the element is not in the tree */
  public boolean delete(E e) {
    // Locate the node to be deleted and also locate its parent node
    TreeNode<E> parent = null;
    TreeNode<E> current = root;
    while (current != null) {
      if (e.compareTo(current.element) < 0) {
        parent = current;
        current = current.left;
      }
      else if (e.compareTo(current.element) > 0) {
        parent = current;
        current = current.right;
      }
      else
        break; // Element is in the tree pointed by current
    }

    if (current == null)
      return false; // Element is not in the tree

    // Case 1: current has no left children
    if (current.left == null) {
      // Connect the parent with the right child of the current node
      if (parent == null) {
        root = current.right;
      }
      else {
        if (e.compareTo(parent.element) < 0)
          parent.left = current.right;
        else
          parent.right = current.right;
      }
    }
    else {
      // Case 2: The current node has a left child
      // Locate the rightmost node in the left subtree of
      // the current node and also its parent
      TreeNode<E> parentOfRightMost = current;
      TreeNode<E> rightMost = current.left;

      while (rightMost.right != null) {
        parentOfRightMost = rightMost;
        rightMost = rightMost.right; // Keep going to the right
      }

      // Replace the element in current by the element in rightMost
      current.element = rightMost.element;

      // Eliminate rightmost node
      if (parentOfRightMost.right == rightMost)
        parentOfRightMost.right = rightMost.left;
      else
        // Special case: parentOfRightMost == current
        parentOfRightMost.left = rightMost.left;     
    }

    size--;
    return true; // Element inserted
  }

  /** Obtain an iterator. Use inorder. */
  public java.util.Iterator iterator() {
    return inorderIterator();
  }

  /** Obtain an inorder iterator */
  public java.util.Iterator inorderIterator() {
    return new InorderIterator();
  }

  // Inner class InorderIterator
  class InorderIterator implements java.util.Iterator {
    // Store the elements in a list
    private java.util.ArrayList<E> list =
      new java.util.ArrayList<E>();
    private int current = 0; // Point to the current element in list

    public InorderIterator() {
      inorder(); // Traverse binary tree and store elements in list
    }

    /** Inorder traversal from the root*/
    private void inorder() {
      inorder(root);
    }

    /** Inorder traversal from a subtree */
    private void inorder(TreeNode<E> root) {
      if (root == null)return;
      inorder(root.left);
      list.add(root.element);
      inorder(root.right);
    }

    /** Next element for traversing? */
    public boolean hasNext() {
      if (current < list.size())
        return true;

      return false;
    }

    /** Get the current element and move cursor to the next */
    public Object next() {
      return list.get(current++);
    }

    /** Remove the current element and refresh the list */
    public void remove() {
      delete(list.get(current)); // Delete the current element
      list.clear(); // Clear the list
      inorder(); // Rebuild the list
    }
  }

  /** Remove all elements from the tree */
  public void clear() {
    root = null;
    size = 0;
  }
  
  public Object clone() {
    BinarySearchTree<E> tree1 = new BinarySearchTree<E>();
    
    copy(root, tree1);
    
    return tree1;
  }
  
  private void copy(TreeNode<E> root, BinarySearchTree<E> tree) {
    if (root != null) {
      tree.insert(root.element);
      copy(root.left, tree);
      copy(root.right, tree);
    }
  }
  
  // breadth first traversal
  // used same methodology as web crawler; use a queue
  // to manage the traversal and pull elements from the nodes
  // as we move through the tree
  protected List<E> breadthFirstTraverse(TreeNode<E> root)
  {
	  if (root == null) return null;
	  
	  Queue<TreeNode<E>> queue = new LinkedList<TreeNode<E>>();
	  TreeNode<E> next = null;
	  
	  List<E> elements = new ArrayList<E>();
	  
	  queue.add(root);
	  elements.add(root.element);
	  
	  do {
		  next = queue.remove();
		  if (next.left != null) {
			  queue.add(next.left);
			  elements.add(next.left.element);
		  }
		  if (next.right != null) {
			  queue.add(next.right);
			  elements.add(next.right.element);
		  }
	  } while (queue.size() > 0);
	    
	
	  return elements;

  }
  
  // Height of tree - number of nodes in the longest path
  // with help from: 
  // http://stackoverflow.com/questions/575772/the-best-way-to-calculate-the-height-in-a-binary-search-tree-balancing-an-avl
  public int height(TreeNode<E> node)
  {
	  if (node == null) {
		  return 0;
	  } else {
		  return Math.max(height(node.left), height(node.right)) + 1;
	  }
	  
  }
  
  // Number of leaves in tree
  // Walks the tree recursively, returning 0 if the node is not a leaf and
  // returning 1 if it is, and adding the results up
  public int getNumLeaves(TreeNode<E> node)
  {
	  if (node == null) return 0;
	  
	  if (node.left == null && node.right == null) {
		  return 1;
	  } else {
		  int count = 0;
	  
		  count += getNumLeaves(node.left);
		  count += getNumLeaves(node.right);
		  
		  return count;
	  }
	  
  }
  
  // Number of non-leaves in tree
  // Same algorithm as getNumLeaves, with return values swapped
  public int getNumNonLeaves(TreeNode<E> node)
  {
	  if (node == null) return 0;
	  
	  if (node.left == null && node.right == null) {
		  return 0;
	  } else {
		  int count = 1;
	  
		  count += getNumNonLeaves(node.left);
		  count += getNumNonLeaves(node.right);
		  
		  return count;
	  }
  }
  
}