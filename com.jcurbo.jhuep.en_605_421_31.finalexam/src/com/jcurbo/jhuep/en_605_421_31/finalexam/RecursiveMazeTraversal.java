package com.jcurbo.jhuep.en_605_421_31.finalexam;

/* Original code was provided by Deitel.
 
   This code is used for educational purposes only
   in 605.421 Foundations of Algorithms.
   
   The starting maze will look as follows
   
   # # # # # # # # # # # #
   # . . . # . . . . . . #
   x . # . # . # # # # . #
   # # # . # . . . . # . #
   # . . . . # # # . # . .
   # # # # . # . # . # . #
   # . . # . # . # . # . #
   # # . # . # . # . # . #
   # . . . . . . . . # . #
   # # # # # # . # # # . #
   # . . . . . . # . . . #
   # # # # # # # # # # # #
   
   Your traversed maze should look as follows
    
   # # # # # # # # # # # #
   # x x x # x x x x x x #
   x x # x # x # # # # x #
   # # # x # x x x x # x #
   # . . x x # # # x # x x
   # # # # x # . # x # . #
   # . . # x # . # x # . #
   # # . # x # . # x # . #
   # . . . x x x x x # . #
   # # # # # # . # # # . #
   # . . . . . . # . . . #
   # # # # # # # # # # # #
   
*/ 

import java.util.Scanner;

public class RecursiveMazeTraversal
{
   static final int DOWN = 0;
   static final int RIGHT = 1;
   static final int UP = 2;
   static final int LEFT = 3;
   static final int X_START = 2;
   static final int Y_START = 0;
   static int move = 0;
   static char maze[][] =
      { { '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#' },
        { '#', '.', '.', '.', '#', '.', '.', '.', '.', '.', '.', '#' },
        { '.', '.', '#', '.', '#', '.', '#', '#', '#', '#', '.', '#' },
        { '#', '#', '#', '.', '#', '.', '.', '.', '.', '#', '.', '#' },
        { '#', '.', '.', '.', '.', '#', '#', '#', '.', '#', '.', '.' },
        { '#', '#', '#', '#', '.', '#', '.', '#', '.', '#', '.', '#' },
        { '#', '.', '.', '#', '.', '#', '.', '#', '.', '#', '.', '#' },
        { '#', '#', '.', '#', '.', '#', '.', '#', '.', '#', '.', '#' },
        { '#', '.', '.', '.', '.', '.', '.', '.', '.', '#', '.', '#' },
        { '#', '#', '#', '#', '#', '#', '.', '#', '#', '#', '.', '#' },
        { '#', '.', '.', '.', '.', '.', '.', '#', '.', '.', '.', '#' },
        { '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#' } };

   static Scanner scanner = new Scanner( System.in );

   // method calls mazeTraversal with correct starting point and direction
   public void traverse()
   {
      boolean result = mazeTraversal( maze, X_START, Y_START );

      if ( !result )
         System.out.println( "Maze has no solution." );
   } // end method traverse

   // traverse maze recursively
   public boolean mazeTraversal( char maze2[][], int x, int y )
   {
      maze[ x ][ y ] = 'x';
      move++;
      printMaze();

      // if returned to starting location
      if ( x == X_START && y == Y_START && move > 1 )
      {
         System.out.print( "Returned to starting location!" );
         return false;
      } // end if
      // if maze exited
      else if ( mazeExited( x, y ) && move > 1 )
      {
         System.out.println( "Maze successfully exited!" );
         return true;
      }
      else // make next move
      {
         System.out.print( "Enter 'y' to continue, 'n' to exit: " );
         char response = scanner.nextLine().charAt( 0 );

         // if user enters 'n', exit program
         if ( response == 'n' )
            System.exit( 0 );

         // determine where next move should be made
         for ( int count = 0; count < 4; count++ ) 
         {
            // checks to see if the space to the right is free. If 
            // so, moves there and continues maze traversal. If not, 
            // breaks out of switch and tries new value of move in
            // for statement.
            switch ( count )
            {
               case DOWN: // move down

            	  // if at starting position, check to make sure we don't
            	  // move off board
            	  if (x+1 > maze.length) break;

                  // if user can move down without hitting wall
            	  if (maze[x+1][y] != '#' && maze[x+1][y] != 'x') {
            		   System.out.println("no wall down");
            		   if(mazeTraversal(maze, x+1, y) == true)
            			   return true;
            	  }
                  
            	  // end if
   
                  break;
               case RIGHT: // move right
            	   // if at starting position, check to make sure we don't
            	   // move off board
            	   if (y+1 > maze[x].length) break;

                  // if user can move right without hitting wall
            	   if (maze[x][y+1] != '#' && maze[x][y+1] != 'x') {
            		   System.out.println("no wall right");
            		   if(mazeTraversal(maze, x, y+1) == true)
            			   return true;

            	   }
                  
            	   // end if

                  break;
               case UP: // move up
            	   
            	   // if at starting position, check to make sure we don't
            	   // move off board
            	   if (x-1 < 0) break;

                  // if user can move up without hitting wall
            	   if (maze[x-1][y] != '#' && maze[x-1][y] != 'x') {
            		   System.out.println("no wall up");
            		   if (mazeTraversal(maze, x-1, y) == true)
            			   return true;
            	   }
                  
            	  // end if

                  break;
               case LEFT: // move left
            	   
            	   // if at starting position, check to make sure we don't
            	   // move off board
            	   if (y-1 < 0) break;

                  // if user can move left without hitting wall
            	   if (maze[x][y-1] != '#' && maze[x][y-1] != 'x') {
            		   System.out.println("no wall left");
            		   if (mazeTraversal(maze, x, y-1) == true)
            			   return true;
            	   }
                  // end if
            } // end switch
         } // end for statement

         // if no valid moves available
         maze2[ x ][ y ] = '0';

         // recursive backtracking -- if no directions possible,
         // back up to previous method call
         return false;
      } // end else
   } // end method mazeTraversal

   // check if move is valid
   public static boolean validMove( int row, int column )
   {
      return row >= 0 && row <= 11 && column >= 0 && column <= 11 &&
         maze[ row ][ column ] == '.';
   } // end method validMove

   // check if location is on edge of maze
   public static boolean mazeExited( int row, int column )
   {
      return row == 0 || row == 11 || column == 0 || column == 11;
   } // end method mazeExited

   // draw maze
   public void printMaze()
   {
      int x = 5, y = 30;

      // for each space in maze
      for ( int row = 0; row < maze.length; row++ )
      {
         for ( int column = 0; column < maze[ row ].length;
            column++ )
         {
            if ( maze[ row ][ column ] == '0' )
               System.out.print( " ." );
            else
               System.out.print( " " + maze[ row ][ column ] );
         }

         y += 10;
         x = 5;
         System.out.println();
      } // end for

      System.out.println();
   } // end method printMaze
   /*
   public static void main( String args[] )
   {
	   RecursiveMazeTraversal maze = new RecursiveMazeTraversal();
      maze.traverse();
   } // end main
   */
} // end class Maze

