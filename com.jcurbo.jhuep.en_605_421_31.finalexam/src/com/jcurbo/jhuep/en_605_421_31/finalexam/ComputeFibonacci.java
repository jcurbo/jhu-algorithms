/** Original code was provided by "Introduction to Java Programming" 
 * 8th Edition by Y. Daniel Liang, pages 681 - 684
 *
 *  This code is used for educational purposes only
 *  in 605.421 Foundations of Algorithms. 
 * 
 */

// Most of this is Swing boilerplate, go to createFibOutput() for the main part of the code

package com.jcurbo.jhuep.en_605_421_31.finalexam;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;


public class ComputeFibonacci extends JPanel implements ActionListener 
{

	private static final long serialVersionUID = 1L;
	static int fibCeiling = 0;

	JTextField fibField;
	JButton goButton;
	JLabel fibLabel;
	
	static JTextArea fibOutputArea;
	static JScrollPane fibOutputScrollPane;
	
	public ComputeFibonacci()
	{
		
		JFrame frame = new JFrame("Fibonacci Generation");

		fibLabel = new JLabel("Number of Fibonacci numbers to generate:");
	  
		fibField = new JTextField(10);
		fibField.addActionListener(this);
	  
		goButton = new JButton("Go!");
		goButton.setActionCommand("go");
		goButton.addActionListener(this);
		  
		Box topBox = Box.createVerticalBox();
		  
		Box textBox = Box.createHorizontalBox();
		textBox.add(fibLabel);
		textBox.add(fibField);
		  
		topBox.add(textBox);
		topBox.add(goButton);
		  
		add(topBox);
		
		frame.add(topBox);
		 
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setLocationRelativeTo(null); // Center the frame
		frame.pack();
		frame.setVisible(true); 	
		
	  }
 
	  public void actionPerformed(ActionEvent e)
	  {
		  if ("go".equals(e.getActionCommand())) {
				  fibCeiling = Integer.parseInt(fibField.getText());
				  createFibOutput();
			  }
	  }
		  
	  public static void createFibOutput() 
	  {
		  createFibOutputWindow();
		  
		  writeToFibOutput("Starting Fibonacci generation...");
		  writeToFibOutput("Generating " + Integer.toString(fibCeiling) + " Fibonacci numbers.");
		  
		  // here is the actual Fibonacci generation, done iteratively
		  
		  // We start with the first two numbers done manually
		  long f0 = 0;
		  writeToFibOutput(Long.toString(f0));
		  long f1 = 1;
		  writeToFibOutput(Long.toString(f1));
		  long currentFib = 0;
		  
		  // Doing this a while loop so we can use the counter value later on
		  // We start at 3 because we've already 'run' two instances of the generation
		  // process manually
		  int i = 3;
		  while(i <= fibCeiling) {
			  currentFib = f0 + f1;
			  writeToFibOutput(Long.toString(currentFib));
			  f0 = f1;
			  f1 = currentFib;
			  i++;
		  }
		  
		  // Subtract 3 since we started with that index, to get the true
		  // number of times the while loop was ran
		  writeToFibOutput("Iterations: " + Integer.toString(i-3));
		  
	  }
	  
	  public static void createFibOutputWindow()
	  {        
		  JFrame frame = new JFrame("Output");
			
		  fibOutputArea = new JTextArea(25, 100);
		  fibOutputScrollPane = new JScrollPane(fibOutputArea);
		  fibOutputArea.setEditable(false);
		  fibOutputArea.setFont(new Font("monospaced", Font.PLAIN, 12));
			
		  frame.add(fibOutputScrollPane);
					
		  frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		  frame.setLocationRelativeTo(null); // Center the frame
		  frame.pack();
		  frame.setVisible(true); 		
	  }
		
	  public static void writeToFibOutput(String text)
	  {
		  System.out.println(text);
		  fibOutputArea.append(text + "\n");
	  }
	
	/** Main method */
	/*
	public static void main(String args[]) {
	  // Create a Scanner
	  Scanner input = new Scanner(System.in);
	  System.out.print("Enter an index for the Fibonacci number: ");
	  int index = input.nextInt();
	
	  // Find and display the Fibonacci number
	  System.out.println( 
	    "Fibonacci number at index " + index + " is " + fib(index));
	}
	*/
	
	/** The method for finding the Fibonacci number */
	 /*
	public static long fib(long index) {
	  if (index == 0) // Base case
	    return 0;
	  else if (index == 1) // Base case
	    return 1;
	  else  // Reduction and recursive calls
	    return fib(index - 1) + fib(index - 2);
	} */
	
 
  
}
