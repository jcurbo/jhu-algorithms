package com.jcurbo.jhuep.en_605_421_31.finalexam;

import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Stack;

/* Original code was provided by Deitel.
 
   This code is used for educational purposes only
   in 605.421 Foundations of Algorithms.
   
   Modified by James Curbo for HW1.
*/

// Same code from hw1, except with sorting functions removed and list
// reimplemented as a stack instead of a plain list.

// class to represent a Card in a deck of cards
class Card 
{    
   public static enum Face
   { 
	   Ace, Deuce, Three, Four, Five, Six,
	   Seven, Eight, Nine, Ten, Jack, Queen, King;
   };
   
   public static enum Suit { Clubs, Diamonds, Hearts, Spades };

   private final Face face; // face of card
   private final Suit suit; // suit of card
   
   public final int flag; // used for minimax
   
   // two-argument constructor
   public Card( Face cardFace, Suit cardSuit ) 
   {   
       face = cardFace; // initialize face of card
       suit = cardSuit; // initialize suit of card
       flag = 0;
   } // end two-argument Card constructor
   
   // special constructor for minimax
   public Card(int f) {
	   flag = f;
	   suit = null;
	   face = null;
   }
   
   // return face of the card
   public Face getFace() 
   { 
      return face; 
   } // end method getFace

   // return suit of Card
   public Suit getSuit() 
   { 
      return suit; 
   } // end method getSuit

   // return String representation of Card
   public String toString()
   {
      return String.format( "%s of %s", face, suit );
   } // end method toString
} // end class Card

// class DeckOfCards declaration
public class DeckOfCards 
{
   private Stack<Card> list = new Stack<Card>(); // declare Stack that will store Cards

   // set up deck of Cards and shuffle
   public DeckOfCards()
   {

      // populate deck with Card objects
      for ( Card.Suit suit : Card.Suit.values() )  
      {
         for ( Card.Face face : Card.Face.values() )   
         {
        	list.push(new Card(face, suit) );
         } // end for
      } // end for

      Collections.shuffle( list );  // shuffle deck
   } // end DeckOfCards constructor
   
   // adding a function to shuffle the current deck - jcurbo
   public void shuffleCards() 
   {
	   Collections.shuffle(this.list);
   }
   
   // Compares two cards, following the behavior of Comparable's "compareTo" method.
   // (see http://docs.oracle.com/javase/7/docs/api/java/lang/Comparable.html)
   // Returns -1 if the first card is less than the second card.
   // Returns 0 if the cards are identical.
   // Returns 1 if the first card is greater than the second card.
   public static int compareCards(Card first, Card second)
   {
	   // In general, we will be passing the return value of compareTo back up as our own return value.
	   int retval = 0;
	   
	   // if these are special cards, handle appropriately 
	   // MIN_VALUE is always least, MAX_VALUE is always most
	   
	   if (first.flag == -1 || second.flag == 1) {
		   // first is either infinitely smaller than second, or
		   // second is infinitely larger than first
		   return -1;
	   }
	   
	   // Same thing as above, just reversed
	   if (first.flag == 1 || second.flag == -1) {
		   return 1;
	   }
	   
	   // First we compare the suits. If the suit is the same, we need to compare the faces. Otherwise, 
	   // we can just return the value from the suit comparison.
	   int comparedSuits = first.getSuit().compareTo(second.getSuit());

	   if (comparedSuits == 0) {
		   retval = first.getFace().compareTo(second.getFace());
	   } else {
		   retval = comparedSuits;
	   }
	   return retval;
   }
   
   
   // output deck
   public void printCards()
   {
      // display 52 cards in two columns
      for ( int i = 0; i < list.size(); i++ )
         System.out.printf( "%-20s%s", list.get( i ),
            ( ( i + 1 ) % 2 == 0 ) ? "\n" : "" );
   } // end method printCards
   
   public String printCardsToString()
   {
	   String output = "";
	   for (int i = 0; i < list.size(); i++) {
		   output += String.format("%-20s%s", list.get(i), ((i+1) % 4 == 0) ? "\n" : "");
	   }
	   return output;
   }
   
   // deals a card, removing it from the deck
   // assumes a shuffled deck, so pulls the last one off the deck
   // returns null if there are no cards left
   public Card deal()
   {
	   Card c = null;
	   try {
		   c = list.pop();
	   } catch (EmptyStackException e) {
		   return null;
	   }
	   return c;
   }
   
   public int size()
   {
	   return list.size();
   }
   
} // end class DeckOfCards