package com.jcurbo.jhuep.en_605_421_31.finalexam;

import java.awt.Font;
import java.util.Arrays;
import java.util.List;

import javax.swing.*;

public class BST extends JPanel {
	
	public BST ()
	{
		// Swing setup
		JFrame frame = new JFrame("Binary Search Trees");
		
		JTextArea bstArea;
		JScrollPane bstScrollPane;
		
		bstArea = new JTextArea(25, 100);
		bstScrollPane = new JScrollPane(bstArea);
		bstArea.setEditable(false);
		bstArea.setFont(new Font("monospaced", Font.PLAIN, 12));
		
		frame.add(bstScrollPane);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setLocationRelativeTo(null); // Center the frame
		frame.pack();
		frame.setVisible(true); 
		
		// Real BST stuff starts here
		
		// Our list of keys
		//Integer[] keys = {1,4,5,10,16,17,21};
		Integer[] keys = {10,4,5,16,17,1,21};
		bstArea.append("Initial list of keys: " + Arrays.toString(keys) + "\n");
				
		BinarySearchTree<Integer> tree = new BinarySearchTree<Integer>();
		
		// Inserting into the tree
		// Deliberately not using the BinaryTree(E[]) constructor
		// so we can append each key to the JTextArea as it's inserted
		
		// Note - final exam simply said to build a tree from the keys - since they're
		// in sorted order, simply inserting them will produce a worst-case binary tree with
		// all nodes to the right.  Exam didn't say to build a balanced BST, red/black tree etc...
		
		bstArea.append("Inserting keys into tree...\n");
		for (Integer key: keys) {
			tree.insert2(key);
			bstArea.append("Inserted " + key.toString() + "\n");
		}
		
		// Breadth first traversal of tree
		
		List<Integer> elements = tree.breadthFirstTraverse(tree.getRoot());
		
		bstArea.append("\nElements of tree in breadth first traversal order: \n");
		bstArea.append(Arrays.toString(elements.toArray()) + "\n");
		
		// Height of tree
		
		int height = tree.height(tree.getRoot());
		bstArea.append("\nHeight of tree: " + Integer.toString(height) + " nodes\n");
		
		// Number of leaves in tree
		int leaves = tree.getNumLeaves(tree.getRoot());
		bstArea.append("\nNumber of leaves: " + Integer.toString(leaves) + "\n");
		
		// Number of non-leaves in tree
		int nonleaves = tree.getNumNonLeaves(tree.getRoot());
		bstArea.append("\nNumber of non-leaves: " + Integer.toString(nonleaves) + "\n");

		
	}

}
