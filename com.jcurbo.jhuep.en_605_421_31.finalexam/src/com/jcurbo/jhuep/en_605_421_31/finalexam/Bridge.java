package com.jcurbo.jhuep.en_605_421_31.finalexam;

import java.awt.Font;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Bridge {

	public Bridge(int type)
	{
		// Swing setup
//		JFrame frame = new JFrame("Bridge");
//		
//		JTextArea bArea;
//		JScrollPane bScrollPane;
//		
//		bArea = new JTextArea(25, 100);
//		bScrollPane = new JScrollPane(bArea);
//		bArea.setEditable(false);
//		bArea.setFont(new Font("monospaced", Font.PLAIN, 12));
//		
//		frame.add(bScrollPane);
//		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//		frame.setLocationRelativeTo(null); // Center the frame
//		frame.pack();
//		frame.setVisible(true); 
		
		play(type);
		
	}
	
	public void play(int type)
	{
		// The constructor will shuffle the deck (see DeckOfCards.java)
		DeckOfCards deck = new DeckOfCards();
		
		// list containing both hands (aka, game state)
		List<ArrayList<Card>> hands = new ArrayList<ArrayList<Card>>();
		hands.add(new ArrayList<Card>());
		hands.add(new ArrayList<Card>());

				
		// grab first half for max, second half for min
		
		// for (int i = 0; i < deck.size() / 2; i++) {
		for (int i = 0; i < 4 / 2; i++) {
			// 0 is min's hand, 1 is max's hand
			hands.get(0).add(deck.deal());
			hands.get(1).add(deck.deal());
		}
		
		if (type == 0) { // minimax
			minimax(1, hands, 0);
		} else if (type == 1) { // alpha-beta
			alphabeta(1, hands, Integer.MAX_VALUE, Integer.MIN_VALUE, 0);
		}
		
		System.out.println("All done");
		
	}

	public int minimax(int player, List<ArrayList<Card>> hands, int depth) 
	{
		int bestScore = 0;
		int newScore = 0;
		
		depth++;
		
		if (player == 1) // max
		{
			while (hands.get(player).size() > 0) {
				bestScore = scoreCard(hands.get(player).remove(0)); // replace with 'get best card' function? TODO
				
				newScore = minimax(0, hands, depth);
				
				System.out.format("\nAt depth %d\n", depth);
				System.out.format("player 1 (max) played %d\n", bestScore);
				System.out.format("opponent (min) played %d\n", newScore);
				
				if (newScore > bestScore) {

					bestScore = newScore;
					System.out.println("opponent (min) won");
				} else {
					System.out.format("player 1 (max) won\n", player);
				}
				
			} ;
			
			return bestScore;

		} else { // min
			do {
				bestScore = scoreCard(hands.get(player).remove(0)); // replace with 'get best card' function? TODO

				newScore = minimax(1, hands, depth);
				
				System.out.format("\nAt depth %d\n", depth);
				System.out.format("player 0 (min) played %d\n", bestScore);
				System.out.format("opponent (max) played %d\n", newScore);

				if (newScore < bestScore) {
					bestScore = newScore;
					System.out.println("opponent (max) won");
				} else {
					System.out.format("player 0 (min) won\n", player);
				}

				
			} while (hands.get(player).size() > 1);
			
			return bestScore;
		}
		
		
	}
	
	public int alphabeta(int player, List<ArrayList<Card>> hands, int alpha, int beta, int depth)
	{
		
		int newScore = 0;
		
		depth++;
		
		if (player == 1) // max
		{
			while (hands.get(player).size() > 0) {
				alpha = scoreCard(hands.get(player).remove(0)); // replace with 'get best card' function TODO
				
				newScore = alphabeta(0, hands, alpha, beta, depth);		
				
				System.out.format("\nAt depth %d\n", depth);
				System.out.format("player 1 (max) played %d\n", player, alpha);
				System.out.format("opponent (min) played %d\n", newScore);
				
				if (newScore > alpha) {
					alpha = newScore;
					System.out.println("opponent (min) won");
				} else {
					System.out.format("player 1 (max) won\n", player);
				}

			} ;
			
			return alpha;

		} else { // min

			do {
				beta = scoreCard(hands.get(player).remove(0)); // replace with 'get best card' function TODO

				newScore = alphabeta(1, hands, alpha, beta, depth);	
				
				System.out.format("\nAt depth %d\n", depth);
				System.out.format("player 0 (min) played %d\n", player, beta);
				System.out.format("opponent (max) played %d\n", newScore);
				
				if (newScore < beta) {
					beta = newScore;
					System.out.println("opponent (max) won");

				} else {
					System.out.format("player 0 (min) won\n", player);
				}

				
			} while (hands.get(player).size() > 1);
			
			return beta;
		}
		
		
		
	}
	
	// returns the 'score' of a card
	int scoreCard(Card c)
	{
		// scores are calculated based on the enum values
		// ordinal() returns the position of the enum, starting at 0
		// Suits are enumval+1 * 100 (thus club = 100 ... spade = 400)
		// Faces are just their enum value +1.  (ace = 1, ... king = 13)
		// card = ( suit+1 * 100 ) + face+1
		return ( (c.getSuit().ordinal() + 1) * 100 ) + (c.getFace().ordinal() + 1);
	}
	

	
	public Card getRandomCard(List<Card> hand)
	{
		Random rand = new Random();
		int rNum = rand.nextInt(hand.size());
		return hand.remove(rNum);
	}
	

	
}
