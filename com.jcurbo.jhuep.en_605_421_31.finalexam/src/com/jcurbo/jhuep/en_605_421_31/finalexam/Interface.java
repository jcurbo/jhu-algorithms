package com.jcurbo.jhuep.en_605_421_31.finalexam;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class Interface extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	JLabel topLabel;
	
	JRadioButton prob1Button;
	JRadioButton prob2Button;
	JRadioButton prob3Button;
	JRadioButton prob4Button;
	JRadioButton prob5Button;
	JRadioButton prob6Button;

	
	ButtonGroup probGroup;
	JPanel probPanel;
	
	JButton goButton;
	
	JTextArea outputArea;
	JScrollPane outputScrollPane;
	
	private int probNum = 1;
				
	public Interface ()
	{
		
		topLabel = new JLabel("Choose a problem:");
		
		
		prob1Button = new JRadioButton("Problem 1: Recursive maze traversal");
		prob1Button.setActionCommand("prob1");
		prob1Button.addActionListener(this);
		prob1Button.setSelected(true);
		
		prob2Button = new JRadioButton("Problem 2: Fibonacci numbers");
		prob2Button.setActionCommand("prob2");
		prob2Button.addActionListener(this);
		
		prob3Button = new JRadioButton("Problem 3: Fractals");
		prob3Button.setActionCommand("prob3");
		prob3Button.addActionListener(this);
				
		prob4Button = new JRadioButton("Problem 4: Binary search trees");
		prob4Button.setActionCommand("prob4");
		prob4Button.addActionListener(this);
		
		prob5Button = new JRadioButton("Problem 5: Bridge (minimax)");
		prob5Button.setActionCommand("prob5");
		prob5Button.addActionListener(this);
		
		prob6Button = new JRadioButton("Problem 5: Bridge (alphabeta)");
		prob6Button.setActionCommand("prob5-2");
		prob6Button.addActionListener(this);
		
		probGroup = new ButtonGroup();
		probGroup.add(prob1Button);
		probGroup.add(prob2Button);
		probGroup.add(prob3Button);
		probGroup.add(prob4Button);
		probGroup.add(prob5Button);
		probGroup.add(prob6Button);

		
		probPanel = new JPanel(new GridLayout(0,1));
		probPanel.add(topLabel);
		probPanel.add(prob1Button);
		probPanel.add(prob2Button);
		probPanel.add(prob3Button);
		probPanel.add(prob4Button);
		probPanel.add(prob5Button);
		probPanel.add(prob6Button);

		probPanel.setAlignmentX(CENTER_ALIGNMENT);
		
		goButton = new JButton("Go!");
		goButton.setActionCommand("go");
		goButton.addActionListener(this);
		
		Box topBox = Box.createVerticalBox();
		
		topBox.add(probPanel);
		topBox.add(goButton);
		
		add(topBox);
	
		
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if ("prob1".equals(e.getActionCommand())) {
			probNum = 1;
		} else if ("prob2".equals(e.getActionCommand())) {
			probNum = 2;
		} else if ("prob3".equals(e.getActionCommand())) {
			probNum = 3;
		} else if ("prob4".equals(e.getActionCommand())) {
			probNum = 4;
		} else if ("prob5".equals(e.getActionCommand())) {
			probNum = 5;
		} else if ("prob5-2".equals(e.getActionCommand())) {
			probNum = 6;
		} else if ("go".equals(e.getActionCommand())) {
						
			beginProblem();
			
		}
	}
	
	public void beginProblem()
	{
	    
	    switch (probNum) {
	    case 1: 
	    	RecursiveMazeTraversal maze = new RecursiveMazeTraversal();
	    	maze.traverse();
	    	break;
	    case 2: 
	    	ComputeFibonacci f = new ComputeFibonacci();
	    	break;
	    case 3:
	    	Fractal demo = new Fractal();
	    	break;
	    case 4:
	    	BST b = new BST();
	    	break;
	    case 5:
	    	Bridge br0 = new Bridge(0);
	    	break;
	    case 6:
	    	Bridge br1 = new Bridge(1);
	    	break;
	    }
	}
	
	public void createOutputWindow()
	{        
		JFrame frame = new JFrame("Output");
		
		outputArea = new JTextArea(25, 100);
		outputScrollPane = new JScrollPane(outputArea);
		outputArea.setEditable(false);
		outputArea.setFont(new Font("monospaced", Font.PLAIN, 12));
		
		frame.add(outputScrollPane);
				
	    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    frame.setLocationRelativeTo(null); // Center the frame
	    frame.pack();
	    frame.setVisible(true); 		
	}
	
	public void writeToOutput(String text)
	{
		System.out.println(text);
		outputArea.append(text + "\n");
	}
	
	public static void createInterface()
	{
		JFrame frame = new JFrame("605.421 Final Exam");
		
		Interface probInterface = new Interface();
		
		frame.add(probInterface, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    frame.setSize(800, 200);
	    frame.setLocationRelativeTo(null); // Center the frame
	    frame.pack();
	    frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createInterface();
            }
        });

	}

}
