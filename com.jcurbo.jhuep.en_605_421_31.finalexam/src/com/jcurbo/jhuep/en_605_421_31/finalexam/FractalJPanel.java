/** Original code was provided by Deitel.
 
   This code is used for educational purposes only
   in 605.421 Foundations of Algorithms.
*/
package com.jcurbo.jhuep.en_605_421_31.finalexam;


import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

public class FractalJPanel extends JPanel
{
	// Drawing the "Lo Fractal" using recursion.
   private Color color; // stores color used to draw fractal
   private int level;   // stores current level of fractal

   private final int WIDTH = 700;  // defines width of JPanel
   private final int HEIGHT = 700; // defines height of JPanel

   // set the initial fractal level to the value specified
   // and set up JPanel specifications
   public FractalJPanel( int currentLevel )
   {
      level = currentLevel; // set initial fractal level
      setBackground( Color.WHITE );
      setPreferredSize( new Dimension( WIDTH, HEIGHT ) );
   } // end constructor FractalJPanel

   // draw fractal recursively
   public void drawFractal( int level, int xA, int yA, int xB, 
      int yB, Graphics g )
   {
      // base case: draw a line connecting two given points
      if ( level == 0 ) 
         g.drawLine( xA, yA, xB, yB );
      else // recursion step: determine new points, draw next level
      {	
         // calculate midpoint between (xA, yA) and (xB, yB)
         int xC = ( xA + xB ) / 2;
         int yC = ( yA + yB ) / 2;

         // calculate the fourth point (xD, yD) which forms an 
         // isosceles right triangle between (xA, yA) and (xC, yC) 
         // where the right angle is at (xD, yD)
         int xD = xA + ( xC - xA ) / 2 - ( yC - yA ) / 2;
         int yD = yA + ( yC - yA ) / 2 + ( xC - xA ) / 2;
         
         // recursively draw the Fractal
         drawFractal( level - 1, xD, yD, xA, yA, g );
         drawFractal( level - 1, xD, yD, xC, yC, g );
         drawFractal( level - 1, xD, yD, xB, yB, g );  
      } // end else
   } // end method drawFractal
   
   // start the drawing of fractal
   public void paintComponent( Graphics g )
   {
      super.paintComponent( g );
      
      // Center of circle
      int x0 = 250;
      int y0 = 250;
      
      // Radius of circle
      int r = 200;
      
      // draw fractal pattern
      g.setColor( Color.BLUE );
      drawFractal( level, x0, y0, polar2x(x0, r, 72), polar2y(y0, r, 72), g ); 
      g.setColor( Color.RED );
      drawFractal( level, x0, y0, polar2x(x0, r, 72*2), polar2y(y0, r, 72*2), g );
      g.setColor( Color.GREEN );
      drawFractal( level, x0, y0, polar2x(x0, r, 72*3), polar2y(y0, r, 72*3), g );
      g.setColor( Color.MAGENTA );
      drawFractal( level, x0, y0, polar2x(x0, r, 72*4), polar2y(y0, r, 72*4), g );
      g.setColor( Color.ORANGE );
      drawFractal( level, x0, y0, polar2x(x0, r, 72*5), polar2y(y0, r, 72*5), g ); 

   } // end method paintComponent
   
   public int polar2x(int x0, int r, int degrees)
   {
	   return (int) (x0 + r * Math.cos(degrees * Math.PI / 180));
   }
   
   public int polar2y(int y0, int r, int degrees)
   {
	   return (int) (y0 + r * Math.sin(degrees * Math.PI / 180));
   }

   // set the drawing color to c
   public void setColor( Color c )
   {
      color = c;
   }
    
   // set the new level of recursion
   public void setLevel( int currentLevel )
   {
      level = currentLevel;
   }

   // returns level of recursion 
   public int getLevel()
   {
      return level;
   }
}; // end class FractalJPanel