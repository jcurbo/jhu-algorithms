# Code for Fundamentals of Algorithms

James Curbo <jcurbo@pobox.com>

This repository holds source code and reports for my work in the class
_Fundamentals of Algorithms_ in Johns
Hopkins University's Engineering for Professionals program.

The main coursework is done in Java and consists of Eclipse projects.

Reports are written in LaTeX.

# Summary of Exercises

This is a summary of the homework exercises and projects written for the class, in chronological order.

## Homework 1: Sorting Algorithms
- Provide a brief description of selection, insertion, and merge sort, and provide pseudocode.
- Modify provided card deck code to add all three sorting algorithms.  Allow user to select which sorting algorithm to use.

## Homework 2: N-Queens
- Write a program to solve the n-queens problem for 6, 8, 10, or 12 queens.
- Construct a recursion tree to calculate the cost of the algorithms.
- Add functionality to randomly select the first queen's starting place via two different randomization methods.

## Homework 3: Tic-Tac-Toe
- Implement a random move algorithm using two different methods.

## Project 1: Web Crawler / Tic-Tac-Toe part 2
- Write a short paper on web crawling methods.
- Modify Homework 3 to add a "best move" and recursive selection algorithm to the AI.

## Homework 4: Huffman Encoding / Web Crawler part 2
- Implement a Huffman encoder/decoder.
- Implement an interface for a breadth-first web crawler.

## Homework 5: Web Crawler part 3
- Implement the actual crawling engine for the web crawler.

## Project 2: Tic-Tac-Toe part 3 / Web Crawler part 4
- Add min-max and alpha-beta pruning decision methods to tic-tac-toe AI.
- Add recursive search to web crawler.

## Final Exam
- Write a recursive maze traversal method.
- Write code to generate Fibonacci numbers in an iterative manner.
- Write code to generate fractals.
- Implement binary search trees.
- Implement mix-max and alpha-beta pruning for a bridge card game.

# License
See LICENSE file.