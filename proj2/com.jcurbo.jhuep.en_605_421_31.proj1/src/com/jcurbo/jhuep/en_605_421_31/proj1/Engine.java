package com.jcurbo.jhuep.en_605_421_31.proj1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.jsoup.*;
import org.jsoup.nodes.*;
import org.jsoup.select.*;

import com.jcurbo.jhuep.en_605_421_31.proj1.Crawler.searchType;

public class Engine {
	
	private int maxSearchDepth;
	private searchType searchAlg;
	private Interface interfaceInstance;
	
	// Constructor that takes in depth and algorithm
	public Engine(int desiredDepth, searchType s, Interface i)
	{
		interfaceInstance = i;
		maxSearchDepth = desiredDepth;
		searchAlg = s;
	}
	
	// TODO remove later
	public Engine(Interface i) 
	{
		interfaceInstance = i;	
	}
	
	public void start(String startURL)
	{
		// TODO - change crawler based on options - searchAlg
		switch (searchAlg) {
	    case BFS: 
			crawlBFS(startURL);
	    	break;
	    case DFS:
	    	crawlDFS(startURL);
	    	break;
	    case IDDFS:
	    	crawlIDDFS(startURL);
	    	break;
	    case IDDFS2:
	    	crawlIDDFS_2(startURL);
	    	break;
	    case All:
			crawlBFS(startURL);
	    	crawlDFS(startURL);
	    	crawlIDDFS(startURL);
	    	crawlIDDFS_2(startURL);
	    	break;
	    }
		
	}

	public List<String> fetchPageLinks(String page)
	{
		Document doc = null;
		List<String> listLinks = new ArrayList<String>();
		
		try {
			doc = Jsoup.connect(page).get();
		} catch (IOException e) {
			System.out.println("Couldn't connect to URL: " + e);
			return listLinks;
		} 
		
		Elements links = doc.select("a[href]");
		
		for (Element link: links) {
			if (link.attr("abs:href").startsWith("http") == true) {
				listLinks.add(link.attr("abs:href"));
			}
		}
		
		return listLinks;
	}
	
	
	// breadth first crawler
	public void crawlBFS(String page)
	{
		int curDepth = 0;
		int counter = 0;
		
		long startTime = System.currentTimeMillis();
		long startMem = Runtime.getRuntime().freeMemory();

		
		Queue<String> queue = new LinkedList<String>();
		List<String> links = new ArrayList<String>();
		String next = "";
		
		interfaceInstance.writeToOutput("Crawling at starting page " + page + " with depth " + Integer.toString(maxSearchDepth));
		interfaceInstance.writeToOutput("--------------------------------------------------------------------------------\n");

		
		// seed the queue with the starting page
		queue.add(page);
		counter++;
		
		do {
			// Check the depth we're at, and break out of the loop if we're above the desired maximum depth
			if (curDepth > maxSearchDepth) {
				break;
			}
			
			// Pull the next page out of the queue
			next = queue.remove();
			
			interfaceInstance.writeToOutput("Fetching and parsing this page: " + next);
			
			// Fetch and parse the page and add any links to the queue
			links = fetchPageLinks(next);
			queue.addAll(links);
			
			// Print out links
			for (String link : links) {
				interfaceInstance.writeToOutput("Link detected: " + link);
			}	
			
			curDepth++;
			counter += links.size();
		} while (queue.size() > 0);
		
		// Reporting
		long estimatedTime = System.currentTimeMillis() - startTime;
		long usedMem = startMem - Runtime.getRuntime().freeMemory();

		interfaceInstance.writeToOutput("Total pages crawled: " + Integer.toString(counter));
		interfaceInstance.writeToOutput("Elapsed time: " + Integer.toString((int) estimatedTime) + " ms");
		interfaceInstance.writeToOutput("Estimated memory usage: " + Integer.toString((int) usedMem / 1024) + " MB");
		interfaceInstance.writeToOutput("--------------------------------------------------------------------------------\n");
		
	}
	
	// iterative deepening depth first search
	// TODO currently this is just regular DFS with a depth limit, not iteratively deepening
	public int crawlRecIDDFS(String page, int curDepth, int maxDepth)
	{
		int counter = 0;
		
		
		if (curDepth <= maxSearchDepth) {	
			interfaceInstance.writeToOutput("Crawling this page: " + page);
			
			curDepth++;
	
			List<String> links = fetchPageLinks(page);
			counter++;
			
			for (String link: links) {
				interfaceInstance.writeToOutput("Link detected: " + link);
				counter += crawlRecIDDFS(link, curDepth, maxDepth);
			}
		}
		return counter;
		
	}
	
	public int crawlRecIDDFS_2(String page, int depth)
	{
		int counter = 0;
		
		if (depth >= 0 ) {
			interfaceInstance.writeToOutput("Crawling this page: " + page);

			List<String> links = fetchPageLinks(page);
			counter++;
			
			for (String link: links) {
				interfaceInstance.writeToOutput("Link detected: " + link);

				counter += crawlRecIDDFS_2(link, depth-1);

			}
		}
		return counter;
	}
	
	public void crawlDFS(String page)
	{
		long startTime = System.currentTimeMillis();
		long startMem = Runtime.getRuntime().freeMemory();

		
		int counter = crawlRecIDDFS(page, 0, maxSearchDepth);
		
		long estimatedTime = System.currentTimeMillis() - startTime;
		long usedMem = startMem - Runtime.getRuntime().freeMemory();

		interfaceInstance.writeToOutput("Total pages crawled: " + Integer.toString(counter));
		interfaceInstance.writeToOutput("Elapsed time: " + Integer.toString((int) estimatedTime) + " ms");
		interfaceInstance.writeToOutput("Estimated memory usage: " + Integer.toString((int) usedMem / 1024) + " MB");
		interfaceInstance.writeToOutput("--------------------------------------------------------------------------------\n");

	}
	
	public void crawlIDDFS(String page)
	{
		int counter = 0;
		long startTime = System.currentTimeMillis();
		long startMem = Runtime.getRuntime().freeMemory();


		for (int i = 0; i <= maxSearchDepth; i++) {
			counter += crawlRecIDDFS(page, 0, i);
		}
		
		long estimatedTime = System.currentTimeMillis() - startTime;
		long usedMem = startMem - Runtime.getRuntime().freeMemory();


		interfaceInstance.writeToOutput("Total pages crawled: " + Integer.toString(counter));
		interfaceInstance.writeToOutput("Elapsed time: " + Integer.toString((int) estimatedTime) + " ms");
		interfaceInstance.writeToOutput("Estimated memory usage: " + Integer.toString((int) usedMem / 1024) + " MB");
		interfaceInstance.writeToOutput("--------------------------------------------------------------------------------\n");

	}
	
	public void crawlIDDFS_2(String page)
	{
		int counter = 0;
		
		long startTime = System.currentTimeMillis();
		long startMem = Runtime.getRuntime().freeMemory();
		
		int i = 0;
		do {
			counter += crawlRecIDDFS_2(page, i);
			i++;
		} while (i <= maxSearchDepth);
		
	
		long estimatedTime = System.currentTimeMillis() - startTime;
		long usedMem = startMem - Runtime.getRuntime().freeMemory();
		
		interfaceInstance.writeToOutput("Total pages crawled: " + Integer.toString(counter));
		interfaceInstance.writeToOutput("Elapsed time: " + Integer.toString((int) estimatedTime) + " ms");
		interfaceInstance.writeToOutput("Estimated memory usage: " + Integer.toString((int) usedMem / 1024) + " MB");

		interfaceInstance.writeToOutput("--------------------------------------------------------------------------------\n");

	}
	
}
