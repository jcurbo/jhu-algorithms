README for Tic-Tac-Toe Project
605.421 Fundamentals of Algorithms
James Curbo
December 2, 2013

----------------------------------

The solution is an Eclipse project; TicTacToe.java is the main Java file.  (TicTacToe_3.java is the original code)

Upon running, a dialog will come up asking which algorithm to use for the opponent.  
- Best move (the one that came with the original)
- Random via permute-by-sorting
- Random via randomize-in-place
- Recursive (minimax)

Alpha-beta pruning was not implemented; ran out of time and didn't quite understand how to get it going.  

