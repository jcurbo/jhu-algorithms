package com.jcurbo.jhuep.en_605_421_31.hw3;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.*;

public class TicTacToe 
{
	
	public enum moveFuncType { BEST, PBS, RIP, REC };
	static moveFuncType moveFuncUsed = moveFuncType.BEST;

	public static class TTTBoard extends JPanel implements MouseListener
    {
		private static final long serialVersionUID = 1L;
		
		int white;
    	int black;
        final int moves[] = {4, 0, 2, 6, 8, 1, 3, 5, 7};
        boolean won[] = new boolean[1 << 9];
        final int DONE = (1 << 9) - 1;
        final int OK = 0;
        final int WIN = 1;
        final int LOSE = 2;
        final int STALEMATE = 3;
        
		// length of one side of the board
		static int size = 3;
		  

        //Who goes first in the next game?
        boolean first = true;
        
    	//BufferedImage circleImg = null;
    	//BufferedImage crossImg = null;

        public TTTBoard() {
        	
        	/*

        	URL circleURL = getClass().getResource("not.gif");
        	URL crossURL = getClass().getResource("cross.gif");
        	
        	try {
        		circleImg = ImageIO.read(new File(circleURL.getPath()));
        	} catch (IOException e) {
        		System.out.println("Couldn't load image");
        	}
        	
        	try {
        		crossImg = ImageIO.read(new File(crossURL.getPath()));
        	} catch (IOException e) {
        		System.out.println("Couldn't load image");
        	}
        	
        	// ImageIO.read will return null if it can't load the image, instead
        	// of throwing an exception (thanks Java)
        	if (circleImg == null) {
        		System.out.println("Couldn't load image");
        		System.exit(1);
        	}
        	if (crossImg == null) {
        		System.out.println("Couldn't load image");
        		System.exit(1);
        	}
        	*/
        	
    		addMouseListener(this);
    		
    		initWinningPositions();
    		
        }
        
        // Mark all positions with these bits set as winning.
        void isWon(int pos) 
        {
        	for (int i = 0 ; i < DONE ; i++) {
        	    if ((i & pos) == pos) {
        		won[i] = true;
        	    }
        	}
        }
    	
        // Initialize all winning positions.
        // The bitshift operations here create the three positions necessary to win.
        // If you number a 3x3 board like so:
        // 0 1 2
        // 3 4 5
        // 6 7 8
        // You can represent this with a number 1 << x where x is the position.
        // The OR operator merges them together to create a number that represents a combined position.
        void initWinningPositions() {
    		isWon((1 << 0) | (1 << 1) | (1 << 2)); // First row  000 000 111
    		isWon((1 << 3) | (1 << 4) | (1 << 5)); // Second row 000 111 000
    		isWon((1 << 6) | (1 << 7) | (1 << 8)); // Third row  111 000 111
    		isWon((1 << 0) | (1 << 3) | (1 << 6)); // Third col  001 001 001
    		isWon((1 << 1) | (1 << 4) | (1 << 7)); // Second col 010 010 010
    		isWon((1 << 2) | (1 << 5) | (1 << 8)); // First col  100 100 100
    		isWon((1 << 0) | (1 << 4) | (1 << 8)); // Diagonal   100 010 001
    		isWon((1 << 2) | (1 << 4) | (1 << 6)); // Diagonal   001 010 100
        }
        
        /**
         * Compute the best move for white.
         * @return the square to take
         */
        int bestMove(int white, int black) 
        {
        	int bestmove = -1;

        	loop:
        		for (int i = 0 ; i < 9 ; i++) {
        			int mw = moves[i];
        			// These bitshift operations check to see if the position is already taken.
        			// AND (&) returns 1 if both operands are 1 and 0 otherwise. 
        			// white and black are bitmasks representing the current board Xs and Os.
        			if (((white & (1 << mw)) == 0) && ((black & (1 << mw)) == 0)) {
        				// The OR operator | return a bitmask with any bits flipped present in the operands.
        				// This has the effect here of combining the possible move with the current state.
        				int pw = white | (1 << mw);
        				if (won[pw]) {
        					// white wins, take it!
        					return mw;
        				}
        				for (int mb = 0 ; mb < 9 ; mb++) {
        					// Here we are checking to see if the possible move of white (from above)
        					// allows black to win.  
        					if (((pw & (1 << mb)) == 0) && ((black & (1 << mb)) == 0)) {
        						// Same as OR operation above.  (Adds possible black move to current state)
        						int pb = black | (1 << mb);
        						if (won[pb]) {
        							// black wins, take another
        							continue loop;
        						}
        					}
        				}
        				// Neither white nor black can win in one move, this will do.
        				if (bestmove == -1) {
        					bestmove = mw;
        				}
        			}
        		}
        	if (bestmove != -1) {
        		return bestmove;
        	}
        	// No move is totally satisfactory, try the first one that is open
        	for (int i = 0 ; i < 9 ; i++) {
        		int mw = moves[i];
        		if (((white & (1 << mw)) == 0) && ((black & (1 << mw)) == 0)) {
        			return mw;
        		}
        	}
        	// No more moves
        	return -1;
        }
        
        // Recursive algorithm based upon 'goodness' checking as explained at this page:
		// http://erwnerve.tripod.com/prog/recursion/tictctoe.htm
        int recursiveMove(int white, int black)
        {
        	int move = -1;
        	
        	int value = 0;
        	
        	int max = -100;
        	for (int i = 0; i < 9; i++) {
        		if (((white & (1 << i)) == 0) && ((black & (1 << i)) == 0)) {
        			int pw = white | (1 << i);
        			value = goodness(pw, black);
        			if (value > max) {
        				max = value;
        				move = i;
        			}
        		}
        	}
        	return move;
        		
        }
        
        // Checks the 'goodness' of a particular board state, from the point of view of the first argument's
        // board state.  Recursively checks all possibilities.
        int goodness(int player, int opponent)
        {
        	
        	// Check to see if opponent can win, if so, this move is the most terrible
        	if (checkWin(opponent, player) == 1) {
        		return -128;
        	}
        	
        	int max = -200;
        	int value = 0;
        	
        	for (int i = 0; i<9; i++) {
    			if (((player & (1 << i)) == 0) && ((opponent & (1 << i)) == 0)) {
    				int p = player | (1 << i);
    				// Dividing the value by 2 each time makes sooner moves have higher priority
    				value = -goodness(opponent, p) / 2;
    				if (value > max) {
    					max = value;
    				}
    			}
        	}
        	return max;
        }
        
        // checks to see if the specific player being passed in first can win.
        // e.g. call as checkWin(white, black) to see if white will win
        //      call as checkWin(black, white) to see if black will win
        int checkWin(int player, int opponent)
        {
        	int result = 0;
        	
        	for (int i = 0; i < 9; i++) {
    			if (((player & (1 << i)) == 0) && ((opponent & (1 << i)) == 0)) {
    				int pw = player | (1 << i);
    				if (won[pw]) {
    					result = 1;
    				}
    			}
        	}
    		
    		return result;
        	
        }
             
        // implements a comparison function to be used in the PBS algorithm.
        // pos1 and pos2 are integer arrays of the form (priority, position)
        // where priority is a sort key generated randomly.
        // some help from these SO questions:
        // http://stackoverflow.com/questions/9150446/compareto-with-primitives-integer-int
        // http://stackoverflow.com/questions/5393254/java-comparator-class-to-sort-arrays
        public static class PBSPositionCompare implements Comparator<int[]> {
        	@Override
        	public int compare(int[] pos1, int[] pos2)
        	{
        		return ((Integer) pos1[1]).compareTo(pos2[1]);
        	}
        } 
        
        // Implements Permute-by-Sorting to return a random board position.
	  	  // Returns an integer
	  	  private static int randomPosPBS ()
	  	  {

	  		  // 'positions' will represent our board positions, and 
	  		  // 'keys' will be populated with random numbers between
	  		  // 0 and (size^2-1) to be used as sort keys.
	  		  List<Integer> positions = new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8));
	  		  List<Integer> keys = new ArrayList<Integer>(size*size);
	  		  
	  		  // This initializes the sort keys
	  		  Random rand = new Random();
	  		  for (int i = 0; i < (size*size); i++) {
				  int rNum = rand.nextInt((int)Math.pow((size*size), 3));
	  			  keys.add(rNum);
	  		  }
	  		  
	  		  // this creates a List of int[]s where each int[] is
	  		  // [position, sortkey]
			  List<int[]> posList = new ArrayList<int[]>(size*size);
			  for (int i=0; i<(size*size); i++) {
	  			  int[] p = {positions.get(i), keys.get(i)};
	  			  posList.add(p);
	  		  }
	  		  
	  		  // This sorts by the priority value in each subarray.
	  		  Collections.sort(posList, new PBSPositionCompare());
	  		  
	  		  // Now that the list is sorted, we can just grab the first one,
	  		  // and use its position value to generate the return value.
	  		  return posList.get(0)[0];		  
	  	  }
	  	  
		  // Implements Randomize-in-Place to generate a new board position.
		  // Returns an integer
		  private static int randomPosRIP () 
		  {
	  		  List<Integer> positions = new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8));
			  Random rand = new Random();
			  int r;
			  
			  for(int i = 0; i < positions.size(); i++) {
				  // this gives us a number in the range [i .. n]
				  r = rand.nextInt((positions.size() - i) + i);
				  Collections.swap(positions, i, r);
			  }
			  // Now that the list is randomized, just grab the first one
			  return positions.get(0);
		  }
		  

        int randomMove(int white, int black)
        {
        	int move = -1;
        	
        	loop: 
        	for (int i = 0; i < 9; i++) {	
        		// Initial condition skips spots that are already taken
        		if (((white & (1 << i)) == 0) && ((black & (1 << i)) == 0)) {
                	// First, check if a win move is available, and if so, take it
    				int pw = white | (1 << i);
    				if (won[pw]) {
    					// white wins, take it!
    					return i;
    				}
        		
	        		// Second, check if the opponent can make a win move, after our move, and if so, choose not
    				// to make that move (via the continue)
	        		for (int mb = 0 ; mb < 9 ; mb++) {
						if (((pw & (1 << mb)) == 0) && ((black & (1 << mb)) == 0)) {
							int pb = black | (1 << mb);
							if (won[pb]) {
								// black wins, take another
								continue loop;
							}
						}
					}
        		}
        	}
        		        		
        	// Now, grab a random position
        	// Check to make sure it's not already taken, if so, regenerate
        	do {
	        	switch(moveFuncUsed) 
	        	{
	        	case PBS:
	        		move = randomPosPBS();
	        		break;
	        	case RIP:
	        		move = randomPosRIP();
	        		break;
	        	}
        	} while (!(((white & (1 << move)) == 0) && ((black & (1 << move)) == 0)));
        	
        	
        	return move;
        	
        }

        /**
         * User move.
         * @return true if legal
         */
        boolean yourMove(int m) 
        {
        	if ((m < 0) || (m > 8)) {
    	    	return false;
    		}
        	// This checks to see if the user selected a square with an existing mark.
    		if (((black | white) & (1 << m)) != 0) {
    	    	return false;
    		}
    		// This takes the user's move and adds it to the current state.
    		black |= 1 << m;
    		return true;
        }

        /**
         * Computer move.
         * @return true if legal
         */
        boolean myMove() 
        {
        	// If all the bits are flipped on between both players, we're done, the board is full.
        	if ((black | white) == DONE) {
    	    	return false;
    		}
        	
        	int move;
        	
        	switch(moveFuncUsed) {
        		case BEST:
        			move = bestMove(white, black);
        		case PBS:
        		case RIP:
        			// randomMove will check which randomization algorithm to use
        			move = randomMove(white, black);
        		case REC:
        			move = recursiveMove(white, black);
        		default:
        			move = bestMove(white, black);      	   		 
        	}
        	
        	// Adds white's move to the current state.
    		white |= 1 << move;
    		return true;
        } 
        
 
        
        /**
         * Figure what the status of the game is.
         * 'won' is a list of all possible winning states, referenced by the number that represents
         * the bitmask-encoding of that state.  This gives us a convenient lookup into the array.
         */
        int status() 
        {
    		if (won[white]) {
    		    return WIN;
    		}
    		if (won[black]) {
    		    return LOSE;
    		}
    		if ((black | white) == DONE) {
    		    return STALEMATE;
    		}
    		return OK;
        }   

        @Override
		public void paintComponent(Graphics g) {
        	super.paintComponent(g);
        	
        	Graphics2D g2d = (Graphics2D) g;
        	
        	Dimension d = getSize();
        	g.setColor(Color.black);
        	int xoff = d.width / 3;
        	int yoff = d.height / 3;
        	
        	// Draws the grid
        	g.drawLine(xoff, 0, xoff, d.height);
        	g.drawLine(2*xoff, 0, 2*xoff, d.height);
        	g.drawLine(0, yoff, d.width, yoff);
        	g.drawLine(0, 2*yoff, d.width, 2*yoff);
    		
    		for (int i = 0; i < 9; i++) {
    			double xpos = (i % 3 + 0.5) * d.width / 3.0;
    	        double ypos = (i / 3 + 0.5) * d.height / 3.0;
    	        double xr = d.width / 8.0;
    	        double yr = d.height / 8.0;
    	        
    	        // Checks both states (black and white) and draws an image if that square is marked as occupied.
				if ((white & (1 << i)) != 0) {
					g2d.draw(new Ellipse2D.Double(xpos-xr, ypos-yr, xr*2, yr*2));
				} else if ((black & (1 << i)) != 0) {
					g2d.draw(new Line2D.Double(xpos-xr, ypos-yr, xpos+xr, ypos+yr));
			        g2d.draw(new Line2D.Double(xpos-xr, ypos+yr, xpos+xr, ypos-yr));
				}
    	        
    		}
    		
        }

        /**
         * The user has clicked in the program. Figure out where
         * and see if a legal move is possible. If it is a legal
         * move, respond with a legal move (if possible).
         */
        @Override
		public void mouseReleased(MouseEvent e) 
        {
        	int x = e.getX();
        	int y = e.getY();
    	
    		System.out.format("mouse released: x %d y %d\n", x, y);
    	
    		switch (status()) {
    		  case WIN:
    		  case LOSE:
    		  case STALEMATE:
    			// You'll only get here if the previous game was over
    			// (via conditions WIN LOSE or STALEMATE)
    			// So this resets the state and starts the game over
    		    white = black = 0;
    		    if (first) {
    			white |= 1 << (int)(Math.random() * 9);
    		    }
    		    first = !first;
    		    repaint();
    		    return;
    		}

    		// Figure out the row/column
    		Dimension d = getSize();
    		int c = (x * 3) / d.width;
    		int r = (y * 3) / d.height;
    		
    		System.out.format("Mouse clicked in row %d col %d\n", r, c);
    		
    		if (yourMove(c + r * 3)) {
    		    repaint();
    	
    		    switch (status()) {
    		    	case WIN:
			    		showDialog("You lose! X(");
    		    		break;
    		    	case LOSE:
			    		showDialog("You win! XD");
    		    		break;
    		    	case STALEMATE:
			    		showDialog("Stalemate! Everyone loses... X~(");
    		    		break;
    		    	default:
    				if (myMove()) {
    				    repaint();
    				    switch (status()) {
    				      	case WIN:
    				    		showDialog("You lose! X(");
    				      		break;
    				      	case LOSE:
    				    		showDialog("You win! XD");
    				      		break;
    				      	case STALEMATE:
    				    		showDialog("Stalemate! Everyone loses... X~(");
    				      		break;
    				      	default:
    				    }
    				} 
    		    }
    		}
        }

        @Override
		public void mousePressed(MouseEvent e) {
        }

        @Override
		public void mouseClicked(MouseEvent e) {
        }

        @Override
		public void mouseEntered(MouseEvent e) {
        }

        @Override
		public void mouseExited(MouseEvent e) {
        }
        
        public void showDialog(String msg)
        {
        	JOptionPane.showMessageDialog(null, msg, "Result", JOptionPane.INFORMATION_MESSAGE);
        }
        
    }
    
    
    public static class OptionPanel extends JPanel implements ActionListener
    {
    
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		OptionPanel () 
    	{
    		JLabel introLabel = new JLabel("Choose the computer player's algorithm.");
    		introLabel.setAlignmentX(CENTER_ALIGNMENT);
    		
    		JRadioButton bestMoveButton = new JRadioButton("bestMove (default)");
    		bestMoveButton.setActionCommand("bestMove");
    		bestMoveButton.setSelected(true);
    		bestMoveButton.addActionListener(this);
    		
    		JRadioButton randomPBSButton = new JRadioButton("Random Move via Permute-by-Sort");
    		randomPBSButton.setActionCommand("randomMovePBS");
    		randomPBSButton.addActionListener(this);
    		
    		JRadioButton randomRIPButton = new JRadioButton("Random Move via Randomize-in-Place");
    		randomRIPButton.setActionCommand("randomMoveRIP");
    		randomRIPButton.addActionListener(this);
    		
    		JRadioButton recursiveMoveButton = new JRadioButton("Recursive algorithm");
    		recursiveMoveButton.setActionCommand("recursiveMove");
    		recursiveMoveButton.addActionListener(this);
    		
    		ButtonGroup moveGroup = new ButtonGroup();
    		moveGroup.add(bestMoveButton);
    		moveGroup.add(randomPBSButton);
    		moveGroup.add(randomRIPButton);
    		moveGroup.add(recursiveMoveButton);
    		
    		JPanel moveRadioPanel = new JPanel(new GridLayout(0,1));
    		moveRadioPanel.add(bestMoveButton);
    		moveRadioPanel.add(randomPBSButton);
    		moveRadioPanel.add(randomRIPButton);
    		moveRadioPanel.add(recursiveMoveButton);
    		moveRadioPanel.setAlignmentX(CENTER_ALIGNMENT);
    		
    		JButton goButton = new JButton("Go!");
    		goButton.addActionListener(this);
			goButton.setActionCommand("go");
			goButton.setAlignmentX(CENTER_ALIGNMENT);
			
			Box box = Box.createVerticalBox();
			box.add(introLabel);
			box.add(moveRadioPanel);
			box.add(goButton);
			
			add(box, BorderLayout.CENTER);
			
    	}
    	
    	@Override
		public void actionPerformed(ActionEvent e)
		{
			if  ("bestMove".equals(e.getActionCommand())) {
				moveFuncUsed = moveFuncType.BEST;
			} else if ("randomMovePBS".equals(e.getActionCommand())) {
				moveFuncUsed = moveFuncType.PBS;
			} else if ("randomMoveRIP".equals(e.getActionCommand())) {
				moveFuncUsed = moveFuncType.RIP;
			} else if ("recursiveMove".equals(e.getActionCommand())) {
				moveFuncUsed = moveFuncType.REC;
			} else if ("go".equals(e.getActionCommand())) {
				createBoard();
			}
		}
	
    	public void createBoard()
    	{
    		JFrame frame = new JFrame("Tic-Tac-Toe");
    		TTTBoard board = new TTTBoard();
    		frame.add(board, BorderLayout.CENTER);
    	    frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    	    frame.setSize(200, 200);
    	    frame.setLocationRelativeTo(null); // Center the frame
    	    frame.setVisible(true);
    	}
    }
    
    public static void createOptionPanel()
    {
    	JFrame frame = new JFrame("Tic-Tac-Toe Options");
    	OptionPanel op = new OptionPanel();
    	frame.add(op, BorderLayout.CENTER);
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setSize(400, 200);
	    frame.setLocationRelativeTo(null); // Center the frame
	    frame.setVisible(true);
    }
    
	public static void main(String[] args) 
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
			public void run() {
                createOptionPanel();
            }
        });
	}
	
	

}
