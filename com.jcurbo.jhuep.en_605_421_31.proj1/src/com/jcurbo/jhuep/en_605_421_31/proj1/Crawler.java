package com.jcurbo.jhuep.en_605_421_31.proj1;

import java.awt.BorderLayout;
import javax.swing.JFrame;

public class Crawler {
	
	public enum searchType { BFS, DFS, IDDFS, IDDFS2, All };
	//private searchType searchUsed;
	
	//private String rootPage;
	//private int depth;

	/*
	public void setSearchType(searchType desiredType)
	{
		searchUsed = desiredType;
	}
	
	public searchType getSearchType()
	{
		return searchUsed;
	}
	
	public void setRootPage(String desiredPage)
	{
		rootPage = desiredPage;
	}
	
	public String getRootPage()
	{
		return rootPage;
	}
	
	public void setDepth(int d)
	{
		depth = d;
	}
	
	public int getDepth()
	{
		return depth;
	}
	
	*/
	
	///////////////////////////////////////////////
	

	
	public static void createInterface()
	{
		JFrame frame = new JFrame("Web Crawler");
		
		Interface crawlInterface = new Interface();
		
		frame.add(crawlInterface, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    frame.setSize(800, 200);
	    frame.setLocationRelativeTo(null); // Center the frame
	    frame.pack();
	    frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createInterface();
            }
        });

	}

}
