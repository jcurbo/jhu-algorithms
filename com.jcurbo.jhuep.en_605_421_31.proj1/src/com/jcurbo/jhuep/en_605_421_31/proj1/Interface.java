package com.jcurbo.jhuep.en_605_421_31.proj1;

import javax.swing.*;

import com.jcurbo.jhuep.en_605_421_31.proj1.Crawler.searchType;

import java.awt.*;
import java.awt.event.*;

public class Interface extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	JLabel topLabel, rootLabel, depthLabel;
	JTextField rootField;
	
	JSpinner depthSpinner;
	SpinnerModel depthSpinnerModel;
	
	JRadioButton searchBFSButton;
	JRadioButton searchDFSButton;
	JRadioButton searchIDDFSButton;
	JRadioButton searchIDDFS2Button;
	JRadioButton searchAllButton;


	ButtonGroup searchGroup;
	JPanel searchPanel;
	
	JButton goButton;
	
	JTextArea outputArea;
	JScrollPane outputScrollPane;
	
	private searchType searchAlg = searchType.BFS;
			
	public Interface ()
	{
			
		rootLabel = new JLabel("Page to begin crawling from:");
		depthLabel = new JLabel("Depth to search to:");
		
		rootField = new JTextField("http://", 50);
		rootField.addActionListener(this);
		
		// SpinnerNumberModel(initialvalue, min, max, step)
		depthSpinnerModel = new SpinnerNumberModel(0,0,1000,1);
		depthSpinner = new JSpinner(depthSpinnerModel);
		depthLabel.setLabelFor(depthSpinner);
		
		searchBFSButton = new JRadioButton("Breadth-first search");
		searchBFSButton.setActionCommand("searchBFS");
		searchBFSButton.addActionListener(this);
		searchBFSButton.setSelected(true);
		
		searchDFSButton = new JRadioButton("Depth-first search");
		searchDFSButton.setActionCommand("searchDFS");
		searchDFSButton.addActionListener(this);
		
		searchIDDFSButton = new JRadioButton("Iterative deepening depth-first search");
		searchIDDFSButton.setActionCommand("searchIDDFS");
		searchIDDFSButton.addActionListener(this);
				
		searchIDDFS2Button = new JRadioButton("Iterative deepening depth-first search, alternative");
		searchIDDFS2Button.setActionCommand("searchIDDFS2");
		searchIDDFS2Button.addActionListener(this);
		
		searchAllButton = new JRadioButton("Run all search algorithms (!)");
		searchAllButton.setActionCommand("searchAll");
		searchAllButton.addActionListener(this);
		
		searchGroup = new ButtonGroup();
		searchGroup.add(searchBFSButton);
		searchGroup.add(searchDFSButton);
		searchGroup.add(searchIDDFSButton);
		searchGroup.add(searchIDDFS2Button);
		searchGroup.add(searchAllButton);

		
		searchPanel = new JPanel(new GridLayout(0,1));
		searchPanel.add(searchBFSButton);
		searchPanel.add(searchDFSButton);
		searchPanel.add(searchIDDFSButton);
		searchPanel.add(searchIDDFS2Button);
		searchPanel.add(searchAllButton);
		searchPanel.setAlignmentX(CENTER_ALIGNMENT);
		
		goButton = new JButton("Go!");
		goButton.setActionCommand("go");
		goButton.addActionListener(this);
		
		Box topBox = Box.createVerticalBox();
		
		Box textBox = Box.createHorizontalBox();
		textBox.add(rootLabel);
		textBox.add(rootField);
		
		Box spinBox = Box.createHorizontalBox();
		spinBox.add(depthLabel);
		spinBox.add(depthSpinner);
		
		topBox.add(textBox);
		topBox.add(spinBox);
		topBox.add(searchPanel);
		topBox.add(goButton);
		
		add(topBox);
	
		
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if ("searchBFS".equals(e.getActionCommand())) {
			searchAlg = searchType.BFS;
		} else if ("searchDFS".equals(e.getActionCommand())) {
			searchAlg = searchType.DFS;
		} else if ("searchIDDFS".equals(e.getActionCommand())) {
			searchAlg = searchType.IDDFS;
		} else if ("searchIDDFS2".equals(e.getActionCommand())) {
				searchAlg = searchType.IDDFS2;
		} else if ("searchAll".equals(e.getActionCommand())) {
			searchAlg = searchType.All;
		} else if ("go".equals(e.getActionCommand())) {
			
			//crawlerInstance.setRootPage(rootField.getText());
			//String rootPage = rootField.getText();
			//int maxDepth = (int)depthSpinner.getValue();
			
			createOutputWindow();
			
			beginCrawling();
			
		}
	}
	
	public void createOutputWindow()
	{        
		JFrame frame = new JFrame("Web Crawler Output");
		
		outputArea = new JTextArea(25, 100);
		outputScrollPane = new JScrollPane(outputArea);
		outputArea.setEditable(false);
		outputArea.setFont(new Font("monospaced", Font.PLAIN, 12));
		
		frame.add(outputScrollPane);
				
	    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    frame.setLocationRelativeTo(null); // Center the frame
	    frame.pack();
	    frame.setVisible(true); 		
	}
	
	public void beginCrawling()
	{
		String rootPage = rootField.getText();
		int maxDepth = (int)depthSpinner.getValue();
	    
	    writeToOutput("Root page for crawler: " + rootPage);
	    writeToOutput("Desired search depth: " + Integer.toString(maxDepth));
	    
	    String searchDesc = "";
	    switch (searchAlg) {
	    case BFS: 
	    	searchDesc = "Breadth-first search";
	    	break;
	    case DFS: 
	    	searchDesc = "Depth-first search";
	    	break;
	    case IDDFS:
	    	searchDesc = "Iterative deepening depth-first search";
	    	break;
	    case IDDFS2:
	    	searchDesc = "Iterative deepening depth-first search, alternative implementation";
	    	break;
	    case All:
	    	searchDesc = "Run all search algorithms in series";
	    	break;
	    }
	    writeToOutput("Search algorithm in use: " + searchDesc);
	    writeToOutput("--------------------------------------------------------------------------------\n");
	    
		//Engine eng = new Engine(this);
	    Engine eng = new Engine(maxDepth, searchAlg, this);
		eng.start(rootPage);
	}
	
	public void writeToOutput(String text)
	{
		System.out.println(text);
		outputArea.append(text + "\n");
	}

	
	
}
