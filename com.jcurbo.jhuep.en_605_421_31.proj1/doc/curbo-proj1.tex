% setup page to suit conference specification using fancyhdr
\documentclass[conference,letterpaper]{IEEEtran}
\usepackage{fancyhdr}

%\setlength{\paperwidth}{215.9mm}
%\setlength{\hoffset}{-9.7mm}
%\setlength{\oddsidemargin}{0mm}
%\setlength{\textwidth}{184.3mm}
%\setlength{\columnsep}{6.3mm}
%\setlength{\marginparsep}{0mm}
%\setlength{\marginparwidth}{0mm}
%
%\setlength{\paperheight}{279.4mm}
%\setlength{\voffset}{-7.4mm}
%\setlength{\topmargin}{0mm}
%\setlength{\headheight}{0mm}
%\setlength{\headsep}{0mm}
%\setlength{\topskip}{0mm}
%\setlength{\textheight}{235.2mm}
%\setlength{\footskip}{12.4mm}

%\setlength{\parindent}{1pc}

\usepackage{algpseudocode}

\usepackage{tabularx}

\usepackage{cite}     

\usepackage{url}       

\usepackage{amssymb}
\usepackage{amsmath}   % From the American Mathematical Society
                        % A popular package that provides many helpful commands
                        % for dealing with mathematics. Note that the AMSmath
                        % package sets \interdisplaylinepenalty to 10000 thus
                        % preventing page breaks from occurring within multiline
                        % equations. Use:
                        %\interdisplaylinepenalty=2500
                        % after loading amsmath to restore such page breaks
                        % as IEEEtran.cls normally does. 

% V1.6 of IEEEtran contains the IEEEeqnarray family of commands that can
% be used to generate multiline equations as well as matrices, tables, etc.

\usepackage{hyperref}

\usepackage{fancyhdr}

\begin{document}

\title{Web Crawling Using Breadth-First Search Algorithms}

\author{\IEEEauthorblockN{James R. Curbo}
\IEEEauthorblockA{Johns Hopkins University\\
Whiting School of Engineering\\
Engineering for Professionals\\
Email: jcurbo1@jhu.edu}}

\maketitle
\thispagestyle{plain}

\pagestyle{fancy}{
\fancyhf{}
\fancyfoot[R]{}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\begin{abstract}

  This paper outlines a strategy for discovering and following links in HTML documents (aka 'web
  crawling') that uses breadth-first graph traversal algorithms.  Best practices for web
  crawlers will be explored, including policies surrounding their impact on server load and proper
  identification of web crawling clients.  Several architectures for Web crawlers will be
  presented.  A solution implementation will be examined and results of running the solution over a
  test set of Web pages will be analyzed.

\end{abstract}


\begin{IEEEkeywords}
Web crawling, breadth-first search, graph theory
\end{IEEEkeywords}

\section{Introduction}

The World Wide Web is built on the idea of many separate pages connected by hyperlinks.  Discovering
the structure of sites and relationships between the content within pages by following links is a
common use case, typically done as part of a search engine as it indexes content to help users find
what they are looking for.  With the Web undergoing explosive growth over the past two decades,
efficient and timely algorithms for traversing links are needed.  Developers targeting the Web must
look to the area of graph theory for solutions to their problems.

This paper will explore the needs of Web crawling algorithms and issues surrounding their use, both
technologically and socially.  Specifically, the usage of breadth-first graph traversal algorithms
will be described, and a case made for their greater utility over other algorithms.  The
architecture of Web crawlers will be explored, including ways of increasing efficiency through
distributed crawling.  An implementation of a simple Web crawler written in Java will be described,
as well as a setup for testing the crawler to verify its functionality as well as validate the
utility of the breadth-first search algorithm.  Policies surrounding the use of Web crawlers will be
considered, including controlling what pages the crawler visits and strategies for avoiding overload
of server resources.

The remainder of this paper is structured as follows.  In Section 2, the applicable literature will
be reviewed for prior work in this area as well as key insights.  Section 3 will describe the Web
crawling problem, describe the solution architecture, describe the Java implementation and test
setup, and discuss usage of the crawler.  Section 4 will review the findings that arise from testing
and running the crawler using real-world data.  Finally, Section 5 will describe the conclusion reached.

\section{Literature Review}

The World Wide Web was first proposed by Tim Berners-Lee in 1989, however the idea of hyperlinks and
hypertext is older.  The terms ``hypertext'' and ``hypermedia'' were coined by Ted Nelson in 1963 to
describe text or other media (e.g. pictures, video) which references other materials.  His
\textit{Project Xanadu} was the first hypertext system \cite{Nelson:2012qf}.

Tim Berners-Lee had long been interested in hypertext, and had created a hypertext system called
ENQUIRE in 1980 while working for the European Organizational for Nuclear Research
(CERN)\cite{Berners-Lee:qc}.  This system used hypertext to store details of people and software
models and would inform his later work.  By the mid-to-late 1980's, hypertext was a widely known
concept, having been implemented in many ways, including the popular HyperCard software for Apple
computers. Berners-Lee was looking for a way to help physicists at CERN share data, and by 1990 had
created the main building blocks that would make up the Web: HTTP (Hypertext Transfer Protocol) and
HTML (Hypertext Markup Language), including a nascent Web browser/editor and Web server.

The Web turned out to be extremely popular and grew quickly, necessitating the creation of search
engines to index and help people retrieve the content they are looking for.  Once the Web grew too
large for static documents (such as portals) to document its contents, search engines became vital
to using the Web.  Web crawlers are an integral part of the design of search engines.

The Web Crawling Project at the University of California, Los Angeles
(UCLA)\cite{University-of-California:2005wf} has many resources related to Web-crawling research,
publishing papers going back to 1998.  Several of these papers discuss ways to improve the
efficiency of crawlers and look at policies for crawler operators to consider.

Najork and Weiner \cite{Najork:2001cz} discuss the utility of breadth-first crawling and how it can
yield higher-quality pages.  This strategy is backed up in the general case by Boldi et
al\cite{Boldi:2004jw}, who showed that breadth-first search can bring up higher quality pages as
defined by Google's PageRank algorithm.

Carlos Castillo's PhD thesis, ``Effective Web Crawling'' \cite{Castillo:2005qf} is an in depth and exhaustive review of the
state of the art in Web crawling as of 2004.



\section{Methodology}

\subsection{Problem Definition}

The problem generally being considered when looking at Web crawling is trying to understand the
nature and structure of the information contained within the Web.  The Web is composed of documents,
also known as webpages, that are composed of text marked up with HTML.  HTML has a way to delineate
a section of text as a hyperlink that points to either another section of the same document, or a
separate document. In a mathematical sense, the Web can be represented as a directed graph, with
pages representing vertices and links representing directed edges.  Hyperlinks in HTML define a
directed relationship between two pages; a page stores links to other pages, but does not store
information on what pages point to it.  To figure that out, the graph must be traversed fully.

Breadth-first search (BFS) provides one way in which to traverse the Web graph.  Given a starting
vertex in a graph, the BFS algorithm traverses the graph by touching all vertices within a
given distance from the starting point before moving to further vertices.  This provides a uniform way
of traversing the graph that does not go too deep too quickly.  While traversing the graph, the BFS
algorithm creates a \textit{breadth-first tree} where the starting vertex is the root of the tree
and adding each discovered vertex to the tree as it discovers them. 

Breadth-first search is straightforward to define in the context of the Web.  Given a section of the
Web $W$, composed of a set of pages $P$ (representing vertices) and a set of links $L$ (representing
directed edges), a single page $s$ from $P$ is selected as the starting point for the search, or
the \textit{source vertex} in graph theory parlance.  The node $s$ becomes the root of a
breadth-first tree.  The algorithm keeps track of the visibility status of pages by coloring them
white, gray, or black.  All pages start out white, meaning they have not been visited by the
algorithm.  Pages are changed to gray when they are visited but still have adjacent unvisited
(white) pages.  When all adjacent pages are visited, the page is changed to black.

Pseudocode for a breadth-first page crawling algorithm is presented in Figure \ref{bfs}. This is based on the BFS
algorithm by Corman et al., with some simplication \cite[p. 595]{Cormen:2009wd}.  $s$ is the starting page given to the
crawler. \textit{color} is an attribute that stores the current state of the page (white, gray, or
black). \textit{depth} is an attribute that stores the distance of the current page from the
starting page. $Q$ is a first-in, first-out queue that holds the set of gray pages. Enqueue and Dequeue are
functions to modify the queue, and Adjacent is a function that lists all pages adjacent to the
current page, as defined by the presence of links.

\begin{figure}[h]
\caption{Breadth-first search algorithm}\label{bfs}
\begin{algorithmic}[1]
\Procedure{BFS}{$s$}
\State $s.color \gets \textsc{gray}$
%\State $s.\pi \gets \textsc{NIL}$
\State $s.depth \gets 0$
\State $Q \gets \varnothing$
\State \Call{Enqueue}{$Q, s$}
\While{$Q \neq \varnothing$}
\State $u \gets$ \Call{Dequeue}{$Q$}
\For{$each v \in$ \Call{Adjacent}{$u$}}
\If{$v.color == \textsc{white}$}
\State $v.color \gets \textsc{gray}$
\State $v.depth \gets u.depth + 1$
\State \Call{Enqueue}{$Q, v$}
\EndIf
\EndFor
\State $u.color \gets \textsc{black}$
\EndWhile
\EndProcedure
\end{algorithmic}
\end{figure}

\subsection{Policy Considerations}

Castillo \cite[p. 28]{Castillo:2005qf} lays out four areas which drive usage of Web crawlers: 

\begin{enumerate}
\item Selection policy - which pages to download
\item Revisit policy - when should pages be re-checked for changes
\item Politeness policy - how to avoid overloading Web servers
\item Parallelization policy - how to coordinate distributed Web crawlers
\end{enumerate}

These will each be treated in turn.

\subsubsection{Selection Policy}

Because of the size of the Web and the number of links involved, any crawler must be designed in
such a way that it can rapidly and selectively traverse the Web graph.  Selection of traversal
algorithm and use of scoring and ranking systems can help immensely.  Google's popularity is due to
the high utility of its PageRank system\cite{Page:1999yb}, which has proven to be a key factor in its success over
other commercial search engines.  

Dynamic pages, such as those generated by server-side programs and those using Javascript, create
issues that must be considered by crawler designers.  Successfully navigating these types of pages
requires much more intelligence and complexity in the crawler's architecture.  To simplify design of
the crawler, these types of pages may want to be skipped by the crawler's selection algorithm or
only partially parsed, if possible.

\subsubsection{Revisit Policy}

Crawlers that exist to build indexes of pages (especially for search engines) must have the ability
to look at changes in pages to see if anything has changed, or if pages have been deleted, in order
to serve up the ``freshest'' results to users.  Cho and Garcia-Molina \cite[p. 17]{Cho:2003kb}
considered two policies for revisiting pages:

\begin{itemize}
\item Uniform policy - revisit all pages with the same frequency
\item Proportional policy - revisit more often the pages that change more frequently
\end{itemize}

On average, the uniform policy was shown to be superior to the proportional policy.  This was due to
the fact that a rapidly changing page would force revisits a higher rate than a uniformly updating
revisit policy. This general notion can be tweaked for specific use cases, such as when the change
rate of a page is known in advance, which ends up being a mix of the uniform and proportional policies.

\subsubsection{Politeness Policy}

Web crawlers can have a significant impact on Web server resources if not used correctly.  The
revisit policy has a large impact on how often a crawler visits a site, and depending on the amount
and type of content at the site, can use a lot of server resources, causing considerable costs for
the server operators.  As modern Web sites are often complex and use large amounts of media
(pictures and video), crawlers can cause large uses of bandwidth in a short amount of time.  Dynamic
Web sites that are being crawled can cause high rates of CPU and I/O utilization on the server,
impacting performance and blocking human users from accessing the site in some cases.  

Web crawlers must also be designed correctly in order to avoid accidentally causing problems.  Bad
design or errors in coding can cause Web crawlers to get stuck in loops, rapidly request resources,
or otherwise impact a site in a negative fashion. 

Another aspect of a so-called politeness policy is Web crawler identification.  Web clients use the
User-Agent HTTP header \cite{http11header} to identify themselves, including which software is
running, what version, operating system, and other parameters.  Web crawler best practice is to
self-identify as a crawler via this mechanism, so that site operators can control crawler access,
identify which crawlers come to their site, and possibly offer up crawler-specific data such as
sitemaps \cite{sitemap:2008} to assist the crawler in its job.

There have been community driven proposals to limit how Web crawlers operate, most notably the
Robots Exclusion Protocol\cite{robots}, but these policies are opt-in on the crawler side and are
not adhered to by all crawlers.  Various strategies exist on the server side to mitigate bad
crawlers, such as rate limiting and blocking crawlers via User-Agent identification.

\subsubsection{Parallelization Policy}

Parallel and distributed crawlers have special issues that arise during their design and use.  Their
distributed nature can help immensely with the task of quickly traversing large parts of the Web
graph, but a high level of coordination is needed by the crawler's control mechanisms to ensure that
other policies (such as the aforementioned selection, revisit and politeness policies) are not
broken. Cho and Garcia-Molina \cite[p.3]{cho2002parallel} identified two ways in which crawler
processes can communicate in order to coordinate their actions:

\begin{itemize}
\item Dynamic assignment, in which a central agent divides the Web graph into partitions and
  dynamically assigns the partitions to worker processes as they operate
\item Static assignment, in which the Web graph is partitioned before the worker processes begin,
  thus letting them work autonomously
\end{itemize}

\subsection{Solution Architecture}

\subsubsection{Primary Architecture}\label{primary}

The following is a description of a model Web crawler that uses an iterative, breadth-first search
algorithm.  The architecture is composed of the following elements:

\begin{itemize}
\item Interface - The interface is the frontend for the user and the starting point for the crawler.
  From the interface, the crawler obtains the starting page and, if desired by the user, a limit to
  the depth the crawler will traverse.
\item Queue - The queue is where the current list of pages to be traversed are stored.
\item Crawler Engine - The crawler engine is the core of the crawler.  It carries out the actions
  listed in the \textit{while} loop of Figure \ref{bfs}.  The engine takes the next page to crawl
  out of the queue, downloads it, passes it to the parser, and monitors progress of the queue for
  stopping conditions.
\item Parser - The parser takes a page passed to it by the engine, looks for links, and inserts those
  links into the queue for further processing.  In addition, if any other work needs to be done with
  the content of the page (for instance, inserting into an index, looking for specific terms, or
  passing the page content off to another algorithm), the parser would be the place to do that.
\end{itemize}

A typical program flow using the above architecture would go as follows:
\begin{enumerate}
\item The user enters a page URL into the interface.
\item The interface inserts the page URL into the queue and kicks off the crawler engine.
\item The engine enters its main loop and removes the first (and only) entry from the queue, the starting URL.
\item The engine accesses the URL and downloads its content.  This content is passed to the parser.
\item The parser parses the HTML looking for links, and when it finds them, inserts them into the
  queue in-order.
\item The parser returns, and the crawler restarts its loop and draws the next URL from the queue,
  download the content, and passes it to the parser.
\item When the parser reaches pages that have no links, it will have nothing to insert into the
  queue.
\item Eventually, the queue will run out, and the crawler will reach its loop exit condition, and
  return to the interface.
\item The interface will report that it has crawled all available links and the program will exit.
\end{enumerate}

\subsubsection{Alternative Architecture}

An alternative way to traverse the graph is by using recursion instead of a iterative queue.  In
this architecture, the parser would call a new instance of itself for each link it encounters in the
page content.  Each new parser would parse its page content and again call a new copy of itself for
each link.  This approach has benefit in that the parser is highly parallelizable and can be
distributed to a large number of processing nodes as necessary, either via multi-threading or a
remote procedure call interface.  A problem with this is that when done with breadth-first search,
no parsers will return until the furthest limits of the graph are reached.  Depending on the
implementation of the parsing algorithm, the memory needed would grow to (number of vertices $x$
parser per-instance memory usage) and could be more that is available to the system.  To effectively
traverse the graph recursively, the search algorithm must be changed from breadth-first search
(which is an iterative algorithm by design) to one known as \textit{iterative deepening depth-first
  search} (IDDFS).  IDDFS is a normal depth-first search where the depth is constrained and
incremented multiple times.  This is effectively equivalent to a breadth-first search but can be
done recursively. This algorithm would replace the core of the engine component and the parser would
remain the same.  Pseudocode for IDDFS as applied to Web crawling, based on the notional algorithm
as described by Russell and Norvig \cite[p. 89]{Russell:2010fv} combined with the depth-first search
described by Cormen et al. \cite[p. 604]{Cormen:2009wd}, is presented in Figure \ref{iddfs}.

\begin{figure}[h]
\caption{Iterative deepening depth first search algorithm}\label{iddfs}
\begin{algorithmic}[1]
\Procedure{IDDFS}{$s$}
\For{$depth = 0 \to \infty$}
\State \Call{Recursive-Depth-Limited-Search}{$s,depth$}
\EndFor
\EndProcedure
\end{algorithmic}

\begin{algorithmic}[1]
\Procedure{Recursive-Depth-Limited-Search}{$u, depth$}
\If{$depth == 0$}
\State \Return
\EndIf
\State $u.color == \textsc{gray}$
\For{$each v \in$ \Call{Adjacent}{$s$}}
\If{$v.color == \textsc{white}$}
\State \Call{Recursive-Depth-Limited-Search}{$v, depth - 1$}
\EndIf
\EndFor
\State $u.color \gets \textsc{black}$
\EndProcedure
\end{algorithmic}
\end{figure}

\subsection{Solution Implementation}

\subsubsection{Crawler Implementation}

The primary solution architecture described in Section \ref{primary} has been implemented in the
Java programming language.  The solution is implemented in three Java classes as detailed below.

\begin{itemize}
\item \textbf{Crawler.java} contains the main entry point of the program, holds global variables and
  instantiates the main interface.

\item \textbf{Interface.java} defines a Java Swing GUI where the user can enter in the starting
  webpage and the depth to search to, as well as selecting the search algorithm to be used.  Once
  crawling begins, a window appears that contains the diagnostic output of the crawler as it parses
  webpages and follows links.

\item \textbf{Engine.java} contains the entirety of the crawling engine described in the primary
  architecture.  This includes the implementations of the various search algorithms as well as the
  code that communicates over Hypertext Transfer Protocol (HTTP) to download webpages and parses
  Hypertext Markup Language (HTML) to look for links to follow.  Four search algorithm
  implementations are provided in this class for the user to choose from: breadth-first search
  (\textit{crawlBFS()}), simple depth-first search (\textit{crawlDFS()}), and two different
  implementations of iterative deepening depth-first search
  (\textit{crawlIDDFS()}/\textit{crawlRecIDDFS()} and
  \textit{crawlIDDFS2()}/\textit{crawlRecIDDFS2()}).

\end{itemize}

The JSoup library \cite{jsoup} was used for both HTML parsing and HTTP communications.

\subsubsection{Testing and Validation Setup}

To verify and validate the functionality of the Web crawler, a testing environment was setup as a
standalone sandbox so that errors in the Web crawler would not impact live Web sites.  This setup
uses a statically generated website with multiple subdirectories and links in order to assure the
integrity of the Web crawler's operation.

The testing environment is a virtual machine provisioned using Virtualbox running on Mac OS X 10.9
as a host operating system.  A virtual machine guest instance using Debian GNU/Linux 7.2 as a base
was created.  The Apache HTTP server was then installed and static web pages loaded.  The static web
pages consisted of a starting page with ten links, each link pointing to a subdirectory also
containing an index page with ten more links to non-existent pages.  The web crawler and test server
were sandboxed together and disconnected from the Internet so that the web crawler could be tested
without causing issues to outside networks and resources.

\section{Findings and Analysis}

To test and compare the search algorithms implemented in the solution, the web crawler was ran
multiple times with the same starting page and with depth two, only varying the search algorithm
chosen.  Each algorithm was ran three times, each from a cold start, and the results of the analysis
can be seen in Table \ref{runtable}.

\begin{table}[h]
\renewcommand{\arraystretch}{1.3}
\caption{Running times of search algorithms}
\label{runtable}
\centering
\begin{tabular}{|l||c|c|c|c|}
    \hline
    ~      & 1       & 2      & 3      & Avg      \\ \hline \hline
    BFS    & 17 ms   & 67 ms  & 19 ms  & 34.3 ms  \\ \hline
    DFS    & 400 ms  & 349 ms & 326 ms & 358.3 ms \\ \hline
    IDDFS  & 1018 ms & 931 ms & 876 ms & 941.7 ms \\ \hline
    IDDFS2 & 318 ms  & 333 ms & 364 ms & 338.3 ms \\ \hline
\end{tabular}

\end{table}

Breadth-first search was the fastest, by an order of magnitude or more over all other algorithms.
Depth-first search and the second iterative deepening DFS implementation performed almost equally,
followed up by the first iterative deepening DFS implementation.  Both IDDFS implmentations use
recursion, and only have one loop within their function bodies to recurse over links in the webpage.
However, IDDFS has one more iteration and passes one more argument to its recursive function, which
may account for the difference.  Extensive profiling of function running times was not performed,
and simple timestamping was done in the outer functions to collect these running times.

\section{Conclusion and Further Work}

In conclusion, breadth-first search is the superior search algorithm for web crawling when speed is
the primary concern.  Breadth-first search's iterative nature also likely uses less memory and
system resources than the recursive depth-first search implementations, and is simpler to implement,
although this conclusion is based solely upon static analysis of the implementations.  Memory
profiling of all search algorithm function implementations would provide more data for forming a
more complete conclusion across all areas of concern.  A more robust and varied testing environment
would provide a more randomized graph of links to put the search algorithms through their paces.

% use section* for acknowledgement
%\section*{Acknowledgment}
% optional entry into table of contents (if used)
% \addcontentsline{toc}{section}{Acknowledgment}
%The authors would like to thank...


% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
% \IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
% \IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% standard IEEE bibliography style from:
% http://www.ctan.org/tex-archive/macros/latex/contrib/supported/IEEEtran/bibtex
\bibliographystyle{IEEEtran.bst}
% argument is your BibTeX string definitions and bibliography database(s)
\bibliography{IEEEabrv,curbo-proj1.bib}

% that's all folks
\end{document} 
