package com.jcurbo.jhuep.en_605_421_31.hw2;

import java.util.List;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import javax.swing.*;

public class NQueens extends JPanel {

	private static final long serialVersionUID = 1L;
	public static int size = 6; // size of the chess board (default)
	
	// holds the queen positions; array index is the row, value is the column.  -99 indicates no queen is in that row
	private ArrayList<Integer> queenList = new ArrayList<Integer>(Collections.nCopies(size, -99));

	// starting position of the first queen
	private int[] startPos = new int[2];
	
	// denotes the two randomization functions available - defaults to Permute by Sorting
	public enum randomFuncType { PBS, RIP };
	randomFuncType randomFuncUsed = randomFuncType.PBS;
		
	public NQueens() {
		add(new OptionPanel(), BorderLayout.CENTER);
	}
	  
	  // checks if a queen can be placed at row i and column j
	  // checks both rows above and below (entire board)
	  private boolean isValidAll(int row, int column) {
		  // check above row
		  for (int i = 1; i <= row; i++) {
			  if (queenList.get(row - i) == column // look up
				  || queenList.get(row - i) == column - i // look up left
				  || queenList.get(row - i) == column + i) // look up right
				  return false;
		  }
		  
		  // check below row
		  for (int i = 1; i < size - row; i++) {
			  if (queenList.get(row + i) == column // look down
				 || queenList.get(row + i) == column - i // look down left
				 || queenList.get(row + i) == column + i) // look down right
				  return false;
		  }
		  		  
		  return true;
			  
	  }
	  
	  // Converts a number to a board position.
	  // Number is between 0 and (size^2 -1) as if the board was
	  // numbered starting at 0 in the top left.
	  // 0 1 2 3 4 5 6 7
	  // 8 9 ...
	  // Returns an integer array, [row, col]
	  private static int[] numToPosition(int num)
	  {
		  int[] pos = {0,0};
		  pos[0] = num / size;
		  pos[1] = num % size;
		  return pos;
	  }
	  
	  // Utility function to generate an array of numbers
	  // from 0 to (size^2 - 1) to represent all positions
	  // on a chessboard with side length equal to 'size'
	  // with help from 
	  // http://stackoverflow.com/questions/10242380/short-and-sweet-sequence-of-integers-in-java
	  private static List<Integer> genBoardPositions()
	  {
		  List<Integer> ret = new ArrayList<Integer>(size*size);
		  for (int i = 0; i < (size*size); ret.add(i++));
		  return ret;
	  }
	  
	  // implements a comparison function to be used in the PBS algorithm.
	  // pos1 and pos2 are integer arrays of the form (priority, position)
	  // where priority is a sort key generated randomly.
	  // some help from these SO questions:
	  // http://stackoverflow.com/questions/9150446/compareto-with-primitives-integer-int
	  // http://stackoverflow.com/questions/5393254/java-comparator-class-to-sort-arrays
	  public static class PositionCompare implements Comparator<int[]> {
		  @Override
		  public int compare(int[] pos1, int[] pos2)
		  {
			return ((Integer) pos1[1]).compareTo(pos2[1]);
		  }
	  } 
	  
	  // Implements Permute-by-Sorting to return a random board position.
	  // Returns an integer array [row, col]
	  // Note: since I'm using integers to represent each square on the board,
	  // I can easily generate a random position by creating a random number
	  // in the range 0 - size^2+1 as done below, then converting it to
	  // row/col format.  But then I wouldn't be permuting-by-sorting.
	  private static int[] randomPosPBS ()
	  {
		  // 'positions' will represent our board positions, and 
		  // 'keys' will be populated with random numbers between
		  // 0 and (size^2-1) to be used as sort keys.
		  List<Integer> positions = genBoardPositions();
		  List<Integer> keys = new ArrayList<Integer>(size*size);
		  
		  // This initializes the sort keys
		  Random rand = new Random();
		  for (int i = 0; i < (size*size); i++) {
			  int rNum = rand.nextInt((int)Math.pow((size*size), 3));
			  keys.add(rNum);
		  }
		  
		  // this creates a List of int[]s where each int[] is
		  // [position, sortkey]
		  List<int[]> posList = new ArrayList<int[]>(size*size);
		  for (int i=0; i<(size*size); i++) {
			  int[] p = {positions.get(i), keys.get(i)};
			  posList.add(p);
		  }
		  
		  // This sorts by the priority value in each subarray.
		  Collections.sort(posList, new PositionCompare());
		  
		  // Now that the list is sorted, we can just grab the first one,
		  // and use its position value to generate the return value.
		  return numToPosition(posList.get(0)[0]);		  
	  }
	  
	  // Implements Randomize-in-Place to generate a new board position.
	  // Returns an integer array [row, col]
	  private static int[] randomPosRIP () 
	  {
		  List<Integer> positions = genBoardPositions();
		  Random rand = new Random();
		  int r;
		  
		  for(int i = 0; i < positions.size(); i++) {
			  // this gives us a number in the range [i .. n]
			  r = rand.nextInt((positions.size() - i) + i);
			  Collections.swap(positions, i, r);
		  }
		  // Now that the list is randomized, just grab the first one
		  return numToPosition(positions.get(0));
	  }
	  

	  
	  // rewritten backtracking search function.  This set the initial case,
	  // which is the random queen position.  It then calls the searchR function to do
	  // the recursion to look for solutions, starting at the next row after the randomly
	  // placed queen. 
	  // Returns true or false depending on if a solution is found.
	  private boolean searchBegin() {
		  
		  // chooses a random queen based on preferred random algorithm
		  if (randomFuncUsed == randomFuncType.PBS) {
			  startPos = randomPosPBS();
		  } else if (randomFuncUsed == randomFuncType.RIP){
			  startPos = randomPosRIP();
		  }
		  
		  // inserts the initial random queen into the list
		  queenList.set(startPos[0], startPos[1]);
		  
		  // begins the search across other rows
		  return searchR(startPos[0] + 1, startPos[0]);
 	  
	  }
	  
	  // Our recursive backtracking placement algorithm.  Uses modular arithmetic to loop around
	  // and ensure the entire board is checked, since random placement can start us in the middle
	  // of the board.  I knew that number theory class would be good for something someday - thanks
	  // Dr. Durand.
	  private boolean searchR (int row, int start) 
	  {
		  // This is a check to see when we're done.  If we get this far, we've checked all rows on the
		  // board and can exit.
		  int trueRow = row % size;
		  if (trueRow == start)
			  return true;
		  
		  // Loops through each column in the row we're checking, placing a queen and checking if
		  // the position is valid.  The recursive call then places the next queen, and so on
		  for (int column = 0; column < size; column++) {
			  queenList.set((trueRow % size), column);
			  //System.out.println(Arrays.toString(queenList.toArray()));

			  if (isValidAll(trueRow, column) && searchR(row + 1, start)) {
				  return true;
			  }
		  }
		  
		  // If we get here, our queen placement was bad and it needs to be picked up.  We will
		  // return to the previous instance of searchR in the stack frame and it will try again.
		  queenList.set((trueRow % size), -99);
		  return false;
	  }
	  
	  // Initial option panel for the app.  Asks user for num of queens and randomization algorithm
	  // to use.
	  class OptionPanel extends JPanel implements ActionListener
	  {

		private static final long serialVersionUID = 1L;

		OptionPanel () 
		  {
			
			JRadioButton num4Button = new JRadioButton("4");
			num4Button.setActionCommand("4");
			
			
		  	JRadioButton num6Button = new JRadioButton("6");
			num6Button.setActionCommand("6");
			num6Button.setSelected(true);
			
			JRadioButton num8Button = new JRadioButton("8");
			num8Button.setActionCommand("8");
			
			JRadioButton num10Button = new JRadioButton("10");
			num10Button.setActionCommand("10");
			
			JRadioButton num12Button = new JRadioButton("12");
			num12Button.setActionCommand("12");
			
			ButtonGroup numGroup = new ButtonGroup();
			numGroup.add(num4Button);
			numGroup.add(num6Button);
			numGroup.add(num8Button);
			numGroup.add(num10Button);
			numGroup.add(num12Button);
			
			num4Button.addActionListener(this);
			num6Button.addActionListener(this);
			num8Button.addActionListener(this);
			num10Button.addActionListener(this);
			num12Button.addActionListener(this);
			
			JRadioButton pbsButton = new JRadioButton("Permute-By-Sorting");
			pbsButton.setActionCommand("PBS");
			pbsButton.setSelected(true);
			
			JRadioButton ripButton = new JRadioButton("Randomize-In-Place");
			ripButton.setActionCommand("RIP");
			
			ButtonGroup randGroup = new ButtonGroup();
			randGroup.add(pbsButton);
			randGroup.add(ripButton);
			
			pbsButton.addActionListener(this);
			ripButton.addActionListener(this);
			
			JPanel numRadioPanel = new JPanel(new GridLayout(1,0));
			numRadioPanel.add(num4Button);
			numRadioPanel.add(num6Button);
			numRadioPanel.add(num8Button);
			numRadioPanel.add(num10Button);
			numRadioPanel.add(num12Button);
			
			JPanel randRadioPanel = new JPanel(new GridLayout(1,0));
			randRadioPanel.add(pbsButton);
			randRadioPanel.add(ripButton);
						
			JButton goButton = new JButton("Go");
			goButton.addActionListener(this);
			goButton.setActionCommand("go");
			
			JLabel queenLabel = new JLabel("Choose number of queens:");
			
			JLabel randLabel = new JLabel("Choose randomization function:");
			
			numRadioPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
			randRadioPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
			goButton.setAlignmentX(Component.CENTER_ALIGNMENT);
			queenLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
			randLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

			
			Box box = Box.createVerticalBox();	
			box.add(queenLabel);
			box.add(numRadioPanel);
			box.add(randLabel);
			box.add(randRadioPanel);
			box.add(goButton);
			
			add(box, BorderLayout.CENTER);

		}
		  
		  public void actionPerformed(ActionEvent e)
			{
				if  ("PBS".equals(e.getActionCommand())) {
					randomFuncUsed = randomFuncType.PBS;
				} else if ("RIP".equals(e.getActionCommand())) {
					randomFuncUsed = randomFuncType.RIP;
				} else if ("4".equals(e.getActionCommand())) {
					size = 4;
					queenList.clear();
					queenList.addAll(Collections.nCopies(size,-99));
				} else if ("6".equals(e.getActionCommand())) {
					size = 6;
					queenList.clear();
					queenList.addAll(Collections.nCopies(size,-99));
				} else if ("8".equals(e.getActionCommand())) {
					size = 8;
					queenList.clear();
					queenList.addAll(Collections.nCopies(size,-99));
				} else if ("10".equals(e.getActionCommand())) {
					size = 10;
					queenList.clear();
					queenList.addAll(Collections.nCopies(size,-99));
				} else if ("12".equals(e.getActionCommand())) {
					size = 12;
					queenList.clear();
					queenList.addAll(Collections.nCopies(size,-99));
				} else if ("go".equals(e.getActionCommand())) {
					createChessBoard();
				}
			}
		  
		  public void createChessBoard()
		  {
			  // searchBegin is a bool - if it returns true we have a solution.
			  // If we have a solution we produce the chessboard.
			  if (searchBegin()) {
				  JFrame frame = new JFrame("ChessBoard");
				  frame.add(new ChessBoard(), BorderLayout.CENTER);
				  frame.setSize(400, 400);
				  frame.setLocationRelativeTo(null); // Center the frame
				  frame.setVisible(true);
			  } else {
				  // No solution was found, so we clear the list and go back to the option panel.
				  queenList.clear();
				  queenList.addAll(Collections.nCopies(size,-99));

				  String dialogText = String.format("No solution could be found for the random position %d, %d.", startPos[0], startPos[1]);
				  JOptionPane.showMessageDialog(null, dialogText, "No solution found", JOptionPane.ERROR_MESSAGE);
			  }
		  }
	  }
	  
	  // Produces the chessboard itself.
	  class ChessBoard extends JPanel 
	  {

		private static final long serialVersionUID = 1L;
		
			private java.net.URL url 
		      = getClass().getResource("queen.jpg");
		    private Image queenImage = new ImageIcon(url).getImage();

		    ChessBoard() 
		    {
		    	setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));      
		    }

		    protected void paintComponent(Graphics g) {
		      super.paintComponent(g);

		      // Paint the queens
		      for (int i = 0; i < size; i++) {
		        int j = queenList.get(i); // The position of the queen in row i
		        //if (j != -1) {
		        	g.drawImage(queenImage, j * getWidth() / size, 
		        			i * getHeight() / size, getWidth() / size, 
		        			getHeight() / size, this);
		        //}
		      }

		      // Draw the horizontal and vertical lines
		      for (int i = 1; i < size; i++) {
		        g.drawLine(0, i * getHeight() / size, 
		          getWidth(), i * getHeight() / size);
		        g.drawLine(i * getWidth() / size, 0, 
		          i * getWidth() / size, getHeight());
		      }
		    }    
	  }
	  
	  public static void main(String[] args) {
		    JFrame frame = new JFrame("NQueens");
		    NQueens app = new NQueens();
		    frame.add(app, BorderLayout.CENTER);
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    frame.setSize(400, 200);
		    frame.setLocationRelativeTo(null); // Center the frame
		    frame.setVisible(true);
		  }
	  
}
