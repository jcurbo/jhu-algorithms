# Fundamentals of Algorithms Homework 2

## Author
James Curbo <jcurbo@pobox.com>

## Assignment Details
Implement n-queens problem for 6, 8, 10, and 12 queen problems.

## Implementation
The assignment has been implemented as a Java application.  It
consists of an Eclipse project with a 'src' directory containing the
Java files and a 'doc' directory containing the report describing the
project written using LaTeX.

### Source Organization
The application consists of just one Java file, NQueens.java.  This is
a heavily modified version of the EightQueens.java included with the
homework.  

### Running the Program
To run the application in Eclipse, open the project and press the Run
button when any part of the project is selected. 

The interface consists of a panel with two groups of radio buttons:
- The first group lets the user select the number of queens to solve for.
- The second lets the user select the randomization algorithm.

The "Go" button begins the search process.  If a solution is found, a
chessboard will be displayed.  If no solution is found, a dialog will
pop up saying so.

Multiple chessboards can be created by closing the chessboard and pressing
"Go" again (with different options if so desired)

## Toolchain Information
- Java version information:
	java version "1.7.0_40"
	Java(TM) SE Runtime Environment (build 1.7.0_40-b43)
	Java HotSpot(TM) 64-Bit Server VM (build 24.0-b56, mixed mode)
- Eclipse version information:
	Eclipse IDE for Java Developers, Kepler Release (build id:
	20130614-0229) running on Windows 7 SP1




