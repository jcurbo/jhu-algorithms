# Fundamentals of Algorithms Homework 1

## Author
James Curbo <jcurbo@pobox.com>

## Assignment Details
Implement selection sort, insertion sort and merge sort.  Use a deck of cards 
to show the sorting algorithms in action.

## Implementation
The assignment has been implemented as a Java application.  It
consists of an Eclipse project with a 'src' directory containing the
Java files and a 'doc' directory containing the report describing the
project written using LaTeX.

### Source Organization
The application consists of two Java files:
- CardSwingConsole.java builds the Swing user interface and is the
  entry point for running the program.
- DeckOfCards.java defines the deck of cards, its interfaces, and the
  sorting algorithms.  It is a modified version of the code provided
  with the assignment.

### Running the Program
To run the application in Eclipse, open the project and press the Run
button when any part of the project is selected. 

The interface consists of a text area and five buttons underneath
with the following functions:
- Print: prints the deck currently in memory.
- Shuffle: shuffles the current deck.
- Selection Sort: sorts the deck via selection sort and displays it.
- Insertion Sort: sorts the deck via insertion sort and displays it.
- Merge Sort: sorts the deck via merge sort and displays it.

The buttons may be pressed in any order.  A deck is created and
shuffled upon program activation.

## Toolchain Information
- Java version information:
	java version "1.7.0_40"
	Java(TM) SE Runtime Environment (build 1.7.0_40-b43)
	Java HotSpot(TM) 64-Bit Server VM (build 24.0-b56, mixed mode)
- Eclipse version information:
	Eclipse IDE for Java Developers, Kepler Release (build id:
	20130614-0229) running on Windows 7 SP1




