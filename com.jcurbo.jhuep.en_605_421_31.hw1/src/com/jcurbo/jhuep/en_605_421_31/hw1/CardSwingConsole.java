package com.jcurbo.jhuep.en_605_421_31.hw1;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class CardSwingConsole extends JPanel implements ActionListener 
{
	private static final long serialVersionUID = 1L;
	protected JTextArea textArea;
	protected JButton printButton, shuffleButton, ssortButton, isortButton, msortButton;
	
	DeckOfCards cards;
	
	public CardSwingConsole(DeckOfCards cards) 
	{
		super(new GridBagLayout());
		
		this.cards = cards;
		
		textArea = new JTextArea(25, 80);
		textArea.setEditable(false);
		textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
		JScrollPane scrollPane = new JScrollPane(textArea);
		
		printButton = new JButton("Print");
		printButton.setActionCommand("print");
		printButton.addActionListener(this);
		
		shuffleButton = new JButton("Shuffle");
		shuffleButton.setActionCommand("shuffle");
		shuffleButton.addActionListener(this);
        
        ssortButton = new JButton("Selection Sort");
        ssortButton.setActionCommand("ssort");
        ssortButton.addActionListener(this);
        
        isortButton = new JButton("Insertion Sort");
        isortButton.setActionCommand("isort");
        isortButton.addActionListener(this);
        
        msortButton = new JButton("Merge Sort");
        msortButton.setActionCommand("msort");
        msortButton.addActionListener(this);
       
        
		GridBagConstraints c = new GridBagConstraints();
        
		c.gridwidth = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.gridx = 0;
        c.gridy = 0;
        add(scrollPane, c);
        
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        add(printButton, c);
        
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;
        add(shuffleButton, c);
        
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 1;
        add(ssortButton, c);
        
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 1;
        add(isortButton, c);
        
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 4;
        c.gridy = 1;
        add(msortButton, c);
              
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		if ("print".equals(e.getActionCommand())) {
			textArea.append("\n\nPrinting current deck\n");
			textArea.append(cards.printCardsToString());
		} else if ("shuffle".equals(e.getActionCommand())) {
			textArea.append("\n\nShuffling deck\n");
			cards.shuffleCards();
			textArea.append(cards.printCardsToString());
		} else if ("ssort".equals(e.getActionCommand())) {
			textArea.append("\n\nSorting deck via selection sort.\n");
			cards.ssortCards();
			textArea.append(cards.printCardsToString());
		} else if ("isort".equals(e.getActionCommand())) {
			textArea.append("\n\nSorting deck via insertion sort.\n");
			cards.isortCards();
			textArea.append(cards.printCardsToString());
		} else if ("msort".equals(e.getActionCommand())) {
			textArea.append("\n\nSorting deck via merge sort.\n");
			cards.msortCards();
			textArea.append(cards.printCardsToString());
		}
	}

	public static void runConsole() 
	{
		// Generate deck of cards.  (note: shuffled during construction)
		DeckOfCards cards = new DeckOfCards();
		
		JFrame frame = new JFrame("Card Sorting Demo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
		CardSwingConsole newContentPane = new CardSwingConsole(cards);
		newContentPane.setOpaque(true);
		frame.setContentPane(newContentPane);
		
		frame.pack();
		frame.setVisible(true);
		
	}
	
	public static void main(String[] args) 
	{
	
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                runConsole();
            }
        });
	}
	
}
