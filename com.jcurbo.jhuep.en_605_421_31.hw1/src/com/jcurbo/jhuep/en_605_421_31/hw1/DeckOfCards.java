package com.jcurbo.jhuep.en_605_421_31.hw1;

/* Original code was provided by Deitel.
 
   This code is used for educational purposes only
   in 605.421 Foundations of Algorithms.
   
   Modified by James Curbo for HW1.
*/

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;

// class to represent a Card in a deck of cards
class Card 
{    
   public static enum Face
   { 
	   Ace, Deuce, Three, Four, Five, Six,
	   Seven, Eight, Nine, Ten, Jack, Queen, King;
   };
   
   public static enum Suit { Clubs, Diamonds, Hearts, Spades };

   private final Face face; // face of card
   private final Suit suit; // suit of card
   
   // two-argument constructor
   public Card( Face cardFace, Suit cardSuit ) 
   {   
       face = cardFace; // initialize face of card
       suit = cardSuit; // initialize suit of card
   } // end two-argument Card constructor
   
   // return face of the card
   public Face getFace() 
   { 
      return face; 
   } // end method getFace

   // return suit of Card
   public Suit getSuit() 
   { 
      return suit; 
   } // end method getSuit

   // return String representation of Card
   public String toString()
   {
      return String.format( "%s of %s", face, suit );
   } // end method toString
} // end class Card

// class DeckOfCards declaration
public class DeckOfCards 
{
   private List< Card > list = new ArrayList<Card>(52); // declare List that will store Cards

   // set up deck of Cards and shuffle
   public DeckOfCards()
   {
      int count = 0; // number of cards

      // populate deck with Card objects
      for ( Card.Suit suit : Card.Suit.values() )  
      {
         for ( Card.Face face : Card.Face.values() )   
         {
        	list.add(count, new Card(face, suit) );
            count++;
         } // end for
      } // end for

      Collections.shuffle( list );  // shuffle deck
   } // end DeckOfCards constructor
   
   // adding a function to shuffle the current deck - jcurbo
   public void shuffleCards() 
   {
	   Collections.shuffle(this.list);
   }
   
   // Compares two cards, following the behavior of Comparable's "compareTo" method.
   // (see http://docs.oracle.com/javase/7/docs/api/java/lang/Comparable.html)
   // Returns -1 if the first card is less than the second card.
   // Returns 0 if the cards are identical.
   // Returns 1 if the first card is greater than the second card.
   public static int compareCards(Card first, Card second)
   {
	   // In general, we will be passing the return value of compareTo back up as our own return value.
	   int retval = 0;
	   
	   // First we compare the suits. If the suit is the same, we need to compare the faces. Otherwise, 
	   // we can just return the value from the suit comparison.
	   int comparedSuits = first.getSuit().compareTo(second.getSuit());

	   if (comparedSuits == 0) {
		   retval = first.getFace().compareTo(second.getFace());
	   } else {
		   retval = comparedSuits;
	   }
	   return retval;
   }
   
   // Sorts the cards via selection sort.
   public void ssortCards()
   {																	// Cost		Times
	   for (int i = 0; i < this.list.size(); i++) {						// c_1		n
		   Card currentMin = this.list.get(i);							// c_2		n-1
		   int currentMinIndex = i;										// c_3		n-1
		   
		   for (int j = i+1; j < this.list.size(); j++) {				// c_4		sum(i=0 to n-1, i+1)
			   if (compareCards(currentMin, this.list.get(j)) > 0) {	// c_5		sum(i=0 to n-1, i)
				   currentMin = this.list.get(j);						// c_6		sum(i=0 to n-1, t_j)
				   currentMinIndex = j;									// c_7 		sum(i=0 to n-1, t_j)
			   }
		   }
		   
		   if (currentMinIndex != i) {									// c_8		n-1
			   Collections.swap(this.list, currentMinIndex, i);			// c_9		t_n
		   }
	   }
   }
   
      
   public void isortCards()
   {																							// 	Cost	Times
	   for (int i = 1; i < this.list.size(); i++) {												//  c_1		i
		   Card currentElement = this.list.get(i);												//  c_2		i-1
		   int k;
		   for (k = i-1; k >= 0 && compareCards(this.list.get(k), currentElement) > 0; k--) {	//  c_3		sum(i=0 to n-1, i-1)
			   this.list.set(k+1, this.list.get(k));											//  c_4		sum(i-0 to n-1, i)
		   }
		   this.list.set(k+1, currentElement);													//  c_5		i-1
	   }
   }
   
   public void msortCards()
   {
	   mergeSort(this.list);
   }
   
   public void mergeSort(List<Card> list)
   {																							// 	Cost	
	   if (list.size() > 1) {																	//  c_1		
		   List<List<Card>> parts = splitListInHalf(list);										//  c_2 or D(n)	
		   mergeSort(parts.get(0));																//  T(n/2)
		   mergeSort(parts.get(1));																//  T(n/2)
		   
		   list.clear();																		//  c_3
		   list.addAll(msortMerge(parts));														//  c_4 or C(n)
	   }
   }
   
   public List<List<Card>> splitListInHalf(List<Card> list)
   {
	   List<List<Card>> parts = new ArrayList<List<Card>>();
	  
	   List<Card> firsthalf = new ArrayList<Card>();
	   firsthalf.addAll(list.subList(0, list.size() / 2 + list.size() % 2));
	   parts.add(firsthalf);

	   List<Card> secondhalf = new ArrayList<Card>();
	   secondhalf.addAll(list.subList(list.size() / 2 + list.size() % 2, list.size()));
	   parts.add(secondhalf);

	   return parts;
   }
   
   public static List<Card> msortMerge(List<List<Card>> parts)
   {
	   List<Card> temp = new ArrayList<Card>(parts.get(0).size() + parts.get(1).size());
	   
	   int current1 = 0;
	   int current2 = 0;
	   int current3 = 0;
	   
	   while (current1 < parts.get(0).size() && current2 < parts.get(1).size()) {
		   if (compareCards(parts.get(0).get(current1), parts.get(1).get(current2)) < 0) {
			   temp.add(current3, parts.get(0).get(current1));
			   current3++;
			   current1++;
		   } else {
			   temp.add(current3, parts.get(1).get(current2));
			   current3++;
			   current2++;
		   }
	   }
	   
	   while (current1 < parts.get(0).size()) {
		   temp.add(current3, parts.get(0).get(current1));
		   current3++;
		   current1++;
	   }
	   while (current2 < parts.get(1).size()) {
		   temp.add(current3, parts.get(1).get(current2));
		   current3++;
		   current2++;
	   }
	   
	   return temp;
   }

   // output deck
   public void printCards()
   {
      // display 52 cards in two columns
      for ( int i = 0; i < list.size(); i++ )
         System.out.printf( "%-20s%s", list.get( i ),
            ( ( i + 1 ) % 2 == 0 ) ? "\n" : "" );
   } // end method printCards
   
   public String printCardsToString()
   {
	   String output = "";
	   for (int i = 0; i < list.size(); i++) {
		   output += String.format("%-20s%s", list.get(i), ((i+1) % 4 == 0) ? "\n" : "");
	   }
	   return output;
   }

   // doesn't really need a main right now
   //public static void main( String args[] )
   //{
   //   DeckOfCards cards = new DeckOfCards();
   //   cards.printCards();
   //} // end main  
} // end class DeckOfCards