\documentclass{article}

\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}

\usepackage[headings]{fullpage}

\usepackage{enumerate}
\usepackage{fontspec}
\setmainfont{Times New Roman}
\setmonofont{Consolas}

\usepackage{clrscode3e}

\usepackage[american]{babel}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}

\fancypagestyle{firststyle}
{
  \fancyhead{}
  \renewcommand{\headrulewidth}{0pt}
  %\fancyfoot[LEO]{Algorithms HW1 - James Curbo}
  %\fancyfoot[REO]{\thepage}
}

\fancyhead[LEO]{Algorithms HW1 - James Curbo}
\fancyhead[REO]{\thepage}
%\fancyfoot[LEO]{Algorithms HW1 - James Curbo}
%\fancyfoot[REO]{\thepage}

\begin{document}
\thispagestyle{firststyle}

\begin{center}
\bfseries James Curbo\\
Johns Hopkins University Engineering for Professionals\\
605.421 Foundations of Algorithms, Fall 2013\\
Homework 1\\
\end{center}

\section{Assignment}
\begin{enumerate}
\item Give a brief description of selection sort, insertion sort and
  merge sort.
\item Provide pseudo-code for selection sort, insertion sort and merge
  sort.
\item Provide a minimum of three references.
\item Write up as a \LaTeX{} document.
\end{enumerate}

\section{Sorting Algorithms}
\subsection{Selection Sort}
\subsubsection{Description}

Selection sort is a simple sorting algorithm that makes use of
multiple comparisons and swaps to sort a list in-place.  Assuming that
the desired order is ascending, the algorithm proceeds as follows.
Step through the list, starting with the first item, looking for the
smallest item.  Once the smallest item is found, swap it with the
first item in the list.  Repeat this process, but start with the
second item in the list, and when the next smallest item is found, swap it
with the second item.  Continue this process until the end of the list
is reached.  Replace the comparison with whatever is necessary for the
problem at hand (smallest, largest, etc).

\subsubsection{Pseudocode}

Here is the pseudocode for selection sort, annotated with the cost and
times for each line.

\begin{codebox}
  \Procname{$\proc{SelectionSort}(A)$}
  \li \For $i \gets 1$ \To $\id{length}[A]$ \RComment $c_1 * n$ where n = \id{length}[A]
  \li \Do
         $\id{smallest} \gets A_i$ \RComment $c_2 * (n-1)$
  \li    \For $j \gets i+1$ \To $\id{length}[A]$ \RComment $c_3 * \sum\nolimits_{i=1}^{n-1} i+1$
  \li    \Do
            \If $A_j < A_i$ \RComment $c_4 * \sum\nolimits_{i=1}^{n-1} i$
  \li       \Then
               $\id{smallest} \gets A_j$ \RComment $c_5 * \sum\nolimits_{i=1}^{n-1} t_i$
            \End
         \End
  \li    $A_i \gets \id{smallest}$ \RComment $c_6 * n-1$
      \End
\end{codebox}

 \subsubsection{Complexity}
In the pseudocode above, $c_1 \ldots c_6$ represent the amount of work
carried out in each associated line of code.  The amount of work is
multiplied by the number of times it is done, since each line is
contained with a loop of some sort.  $t_i$ represents the
possible amount of work done if the conditional statement is
followed.  Because of the way selection sort works, two loops occur:
one which steps over every item in the list (represented by the first
$for$ loop and the variable $i$), and one which steps over the
unsorted section of the list, represented by the inner $for$ loop and
the variable $j$, which is defined in terms of $i$).
%
To calculate the total
amount of work ($T(n)$), the terms are summed, which provides the
following equations:
%
\begin{gather*}
T(n) = c_1n + c_2(n-1) + c_3\sum\nolimits_{i=1}^{n-1} i+1 + c_4\sum\nolimits_{i=1}^{n-1} i + 
c_5\sum\nolimits_{i=1}^{n-1} t_i + c_6(n-1)\\
\downarrow\\
T(n) = c_1n + c_2(n-1) + c_3\frac{(n-1)n}{2} + c_3(n-1) +
c_4\frac{(n-1)n}{2} + c_5\sum\nolimits_{i=1}^{n-1} t_i + c_6(n-1) \\
\downarrow\\
T(n) = c_1n + (c_2 + c_3 + c_6)(n-1) + (c_3 + c_4)\frac{(n-1)n}{2} + c_5\sum\nolimits_{i=1}^{n-1} t_i
\end{gather*}
%
To obtain the worst-case running time scenario, it is assumed that the list is in the state that
requires the most work, which for an ascending selection sort means that the list is sorted in
descending order.  This triggers the conditional statement and its associated work every time.
$t_i$ can be replaced with $i$ in this case and simplify to come up with:
%
\begin{equation*}
T(n) =  c_1n + (c_2 + c_3 + c_6)(n-1) + (c_3 + c_4 +
c_5)\frac{(n-1)n}{2}
\end{equation*}
%
Since the largest term here is $n^2$, the worst-case running time is
$\mathcal{O} (n^2)$.
%
Per \cite[p. 140]{Knu98}, average-case running time of selection sort is
$\Theta (n^2)$. This makes selection sort useful for small amounts of
data, but due to exponential growth, other sorting algorithms are
faster with larger amounts.
%
The best-case running time of selection sort is when the list is
already sorted.  In this case, $t_i$ is set to 0 since the conditional
statement will be skipped every time.  However, two loops still occur,
since every item must be compared to every other one to ensure the
smallest one is found in the inner loop (represented by terms $c_3$
and $c_4$.  This results in a best-case
running time of $\Omega (n^2)$.
%
\cite[p.141]{Knu98} discusses several ways to improve the running
time, including a method known as ``tree selection'' which reduces the
average running time to $\mathcal{O} (n \log n)$.

\subsection{Insertion Sort}
\subsubsection{Description}
Insertion sort is another fairly simple sorting algorithm and is best known as the way playing card
players typically sort cards in their hand.  As before, it is assumed the objective is an
ascending-order sorted list of integers.  Starting with the last item in the list, compare that item
with the previous items in the list until the previous item is smaller than the current item.  Move
the current item to this position.  Perform this process repeatedly until all items in the list have
been sorted. As before, the comparison step can be altered for the case at hand.
\subsubsection{Pseudocode}
Here is the pseudocode for insertion sort, annotated with the cost and
times for each line.

\begin{codebox}
  \Procname{$\proc{InsertionSort}(A)$}
  \li \For $i \gets 2$ \To $\id{length}[A]$ \RComment $c_1 * n$ where n = \id{length}[A]
  \li \Do
         $\id{current} \gets A_i$ \RComment $c_2 * (n-1)$        
         $j \gets i-1$ \RComment $c_3 * (n-1)$
  \li    \While $j > 0$ and $A_j > \id{current}$ 
         \Do \RComment $c_4 * \sum\nolimits_{i=2}^{n} t_j)$
  \li        $A_{j+1} = A_j$ \RComment $c_5 * \sum\nolimits_{i=2}^{n} t_j-1$
  \li        $j \gets j-1$ \RComment $c_6 * \sum\nolimits_{i=2}^{n} t_j-1$
         \End
  \li    $A_{j+1} \gets \id{current}$ \RComment $c_7 * (n-1)$
       \End
\end{codebox}

\subsubsection{Complexity}
Similarly to selection sort, insertion sort has two loops running over the items in the list.
Adding the terms above, the following total cost is derived:
%
\begin{gather*}
T(n) = c_1n + c_2(n-1) + c_3(n-1) + c_4\sum\nolimits_{i=2}^{n} t_j + c_5\sum\nolimits_{i=2}^{n}
(t_j-1) + c_6\sum\nolimits_{i=2}^{n} (t_j-1) + c_7(n-1)\\
\downarrow\\
T(n) = c_1n + (c_2 + c_3 + c_7)(n-1) + c_4\sum\nolimits_{i=2}^{n} t_j + (c_5 +
c_6)\sum\nolimits_{i=2}^{n} (t_j-1)\\
\end{gather*}
%
As before, $t_j$ is altered based on the best or worst case scenarios.  The worst-case running time
for insertion sort is also when the list is sorted in reverse of the desired direction. This is
represented by setting $t_j$ equal to $j$, resulting in the following worst-case total cost:
%
\begin{gather*}
T(n) = c_1n + (c_2 + c_3 + c_7)(n-1) + c_4(\frac{n(n+1)}{2} - 1) + (c_5 + c_6)(\frac{n(n-1)}{2})\\
\downarrow\\
T(n) = (\frac{c_4}{2} + \frac{c_5}{2} + \frac{c_6}{2})n^2 + (c_1 + c_2 + c_3 + \frac{c_4}{2} -
\frac{c_5}{2} - \frac{c_6}{2} + c_7)n
\end{gather*}
Thus, insertion sort's worst-case running time is $\mathcal{O} (n^2)$.
%
\cite[p. 82]{Knu98} states that the average-case running time of insertion sort is $\Theta (n^2)$ as
well. As in the case of selection sort, this is due to the two sets of loops and associated operations
(comparisons and swaps) occurring in conjunction with those loops.
%
Best-case running time of insertion sort is when the list is already sorted.  Setting $t_j$ to 1 (as
only one compare will occur each loop), our total cost is:
\begin{gather*}
T(n) = c_1n + c_2(n-1) + c_3(n-1) + c_4(n-1) + c_7(n-1)\\
\downarrow\\
T(n) = (c_1 + c_2 + c_3 + c_4 + c_7)n - (c_2 + c_3 + c_4 + c_7)
\end{gather*}
%
Here, the largest term is $n$, thus the best-case running time is linear: $\Omega (n)$.  Since the
second loop is avoided in the best case, the work represented by $c_5 and c_6$ is avoided and the
list is only passed over once.

\subsection{Merge Sort}
\subsubsection{Description}
Merge sort is quite different from the other algorithms presented here.  Merge sort is a type of
divide-and-conquer algorithm that uses recursion to break down the work into multiple parts.  The
first step is to divide the list into two equal parts.  These two lists are again split, until each
list is of length 1 and unable to be split again.  At this point, the lists are merged back together
via a special ``merge'' procedure.  This is done until all split lists have been merged back into
only one list.
\subsubsection{Pseudocode}
Here is the pseudocode for the main merge-sort algorithm and merge procedure.
%
\begin{codebox}
  \Procname{$\proc{MergeSort}(A,p,r)$}
  \li \Comment Input is a list $A[p\ldots r]$, where $p$ and $r$ are indices
  \li \If $p < r$
         \Then
  \li    $q = \lfloor(p + r)/2\rfloor$
  \li $\id{merge-sort} (A, p, q)$
  \li $\id{merge-sort} (A, q+1, r)$
  \li $\id{merge} (A, p, q, r)$
\end{codebox}
%
\begin{codebox}
  \Procname{$\proc{Merge}(A, p, q, r)$}
  \li $n_1 \gets q - p + 1$
  \li $n_2 \gets r - q$
  \li \Comment $n_1$ is the index of new array $L$
  \li \Comment $n_2$ is the index of new array $R$
  \li \For $i \gets 1$ \To $n_1$
      \Do
  \li $L_i \gets A_{p+i-1}$
      \End
  \li \For $j \gets 1$ \To $n_2$
      \Do
  \li $R_j \gets A_{q+j}$
      \End
  \li $L_{n_1+1} \gets \infty$
  \li $R_{n_2+1} \gets \infty$
  \li $i \gets 1$
  \li $j \gets 1$
  \li \For $k \gets p$ \To $r$
      \Do
  \li \If $L_i \ge R_j$
      \Then
  \li $A_k \gets L_i$
  \li $i \gets i+1$
  \li \Else
  \li $A_k \gets R_j$
  \li $j \gets j+1$
      \End
      \End
\end{codebox}

\subsubsection{Complexity}
Since merge sort is a divide-and-conquer algorithm, analyzing its complexity involves looking at how
recursion affects work.  Recurrence relations allow representation of the amount of work encompassed
in a recursive procedure.  In general, divide and conquer problems can be represented by this
recurrence relation:
%
\begin{equation*}
T(n) = \begin{cases}
  \Theta (1) & \text{if $n \le c$},\\
  aT(n/b) + D(n) + C(n) & \text{otherwise}.
  \end{cases}
\end{equation*}
%
Here, $T(n)$ is the total work; $n$ is the size of the problem; $c$ is a constant time in which a
straightforward solution can take place for the problem set; $a$ is the number of subproblems that
$T(n)$ is split into; the subproblems are $1/b$ the size of the original problem; $T(1/b)$
represents the work required to solve a subproblem, and $aT(n/b)$ solves all $a$ subproblems; $D(n)$
is the time required to divide the problem into subproblems; and $C(n)$ is the time required to
merge the subproblems back together into the ultimate solution.
%
Analysis of merge sort involves looking at the time involved for the different steps.  The divide
step simply splits the array, which is a constant time operation, so $D(n) = \Theta (1)$. Solving
the subproblems (aka the conquer step) for merge-sort is done in $2T(n/2)$ time, since merge-sort
always splits in two (thus $a = 2, b = 2$).  Combining the results via the merge procedure is a
linear time operation, since there are no nested loops.  Thus $C(n) = \Theta (n)$.  This can be
simplified by assuming $c$ represents the work necessary to solve a problem of time 1, and
substitute it to get the following relation:
%
\begin{equation*}
T(n) = \begin{cases}
  c & \text{if $n = 1$},\\
  2T(n/2) + cn & \text{if $n > 1$}.
\end{cases}
\end{equation*}
%
If this relation is broken out into a recursion tree, $cn$ becomes the amount of work for the root
node, and each branch is successively divided by 2, such that the first level's nodes each have cost
$cn/2$, the second level's nodes each have cost $cn/4$, and so on.  The tree will have $\lg n +1$
levels, so summing the costs results in the total work $cn \lg n + cn$, which is on the order of
$\Theta n \lg n$.

\begin{thebibliography}{1}
\bibitem{Cor09} Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest, and Clifford Stein. Introduction to Algorithms, Third Edition. MIT Press and McGraw-Hill, 2009. ISBN-13: 978-0-262-03384-8.
\bibitem{Cor10} Thomas H. Cormen. The clrscode and clrscode3e packages
  for LaTeX2e, Retrived Jan 2010,
  http://www.cs.dartmouth.edu/~thc/clrscode/.
\bibitem{Knu98} Donald E. Knuth.  The Art of Computer Programming,
  Volume 3, Sorting and Searching, Second Edition.  Addison-Wesley,
  1998.  ISBN: 0-201-89685-0.
\end{thebibliography}

\end{document}
