package com.jcurbo.tutorial1;

public class Hanoi {
	
	static int counter = 0;
	static int disks = 3;

	public static void main(String[] args) {
	
		hanoi("A", "B", "C", disks);
		System.out.format("\nDisks: %d\nMoves: %d", disks, counter);
	}

	public static void hanoi(String source, String temp, String dest, int n) 
	{
		if (n == 0)
			return;
		
		hanoi(source, temp, dest, n-1);
		// move bottom disk from source to dest
		System.out.format("\nmoving disk %d from " + source + " to " + dest, n);
		hanoi(temp, source, dest, n-1);
		counter++;
	
	}
	
}
